<?php get_header(); ?>
	<main id="main-content" class="main classificados categorias pesquisa" role="main">
    <?php 
      get_block('_classificados-menu');
      get_block('_classificados-search');
      $anuncios_encontrados = $wp_query->found_posts;
    ?>
    <div class="results">
      <div class="container-fluid">
        <?php
          if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb('<p id="breadcrumbs">','</p>');
          }
        ?>
        <?php echo '<h2 class="title">'.$anuncios_encontrados.' anúncios encontrados</h2>'; ?>
        <div class="row">
          <?php 
            while ( have_posts() ) : the_post();
              print_anuncio_slide_block('col-12 col-md-3 mb-5', true);
            endwhile; 
          ?>
        </div>
        <?php 
          if (function_exists('wp_pagenavi')) { 
            echo '<div class="clearfix"></div>';
            wp_pagenavi();
          }; 
        ?>
      </div>
    </div>
	</main>
<?php get_footer(); ?>
