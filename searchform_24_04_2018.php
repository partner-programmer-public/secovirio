<?php 
	global $wp_query; 		
	$argsCategories = [
		'show_option_none' => 'Categoria: Todas',
		'hierarchical' => 1
	];
	
	if (is_tax()) {
		// echo '<pre>'. print_r($wp_query, 1) . '</pre>';	
		$argsCategories['taxonomy'] = get_query_var('taxonomy');
		$argsCategories['name'] = get_query_var('taxonomy');
		$argsCategories['show_option_none'] = 0;
	} elseif (is_archive()) {
		$obj = get_post_type_object($wp_query->get('post_type'));	
		// echo '<pre>'. print_r($obj, 1) . '</pre>';
		if ($obj->taxonomies) {
			$argsCategories['taxonomy'] = $obj->taxonomies[0];			
			$argsCategories['name'] = $obj->taxonomies[0];			
		}		
	}

?>
<!-- <form id="search-box" role="search" method="get" class="search-form d-sm-flex" action="<?php echo home_url( '/' ); ?>">
	<label class="sr-only"> Buscar no site </label>
	<?php wp_dropdown_categories($argsCategories); ?>	
	<input type="search" class="search-field form-control icon-search-right" placeholder="" value="<?php echo get_search_query() ?>" name="s" title="BUSCA" />
	<button type="submit" class="btn btn-rosaclaro d-inline-block">BUSCAR</button>
	<input type="hidden" name="post_type" value="<?php echo (isset($obj) ? $obj->name : 'post'); ?>">	
</form> -->