(function ($, root, undefined) {
	$(function () {
		
    'use strict';

		$('body').on('mouseenter mouseleave','.dropdown',function(e){
		  var _d=$(e.target).closest('.dropdown');_d.addClass('show');
		  setTimeout(function(){
		    _d[_d.is(':hover')?'addClass':'removeClass']('show');
		  },300);
		});

		$('#navbarNav').on('show.bs.collapse', function () {
		  $('#headline-menu-wrapper').addClass('d-flex');
		}).on('hide.bs.collapse', function () {
		  $('#headline-menu-wrapper').removeClass('d-flex');
		});

		// banner
		$('.banner').slick({
		  dots: true,
		  fade: true,
		  arrows: false,
		  infinite: true,
		  speed: 800,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  autoplay: true,
		  autoplaySpeed: 3500,		  
		});
		
		$('.slick-last-posts').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 2,
		  slidesToScroll: 2,
		  responsive: [
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        dots: true,
		        arrows: false
		      }
		    }
		  ]
		});


		$('.slick-secoes').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 3,
		  slidesToScroll: 3,
		  responsive: [
		    {
		      breakpoint: 900,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,		        
		        dots: true,
		        arrows: false
		      }
		    }
		  ]
		});

		$('.slick-classificados').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 4,
		  slidesToScroll: 4,
		  responsive: [
		    {
		      breakpoint: 900,
		      settings: {
		        slidesToShow: 3,
						slidesToScroll: 3,
		        dots: false,
		        arrows: false
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,		        
		        dots: false,
		        arrows: false
		      }
		    }
		  ]
		});


		// TR Cliclables
		$('tr[data-url]').click(function() {
	      var href = $(this).data('url');
	      if(href) { window.location = href; }
	    });

		var dt = $('table.table-secovi').DataTable({
			// paging: true,
			pageLength : 10,
			dom: 'rt<"bottom"p><"clear">',
			ordering: false,
			language: { url: siteVars.theme_url + "/languages/datatables.json" },
			initComplete: function () {
		        this.api().columns().every( function () {
		          var column = this,
		          		columnHeader = $(column.header()),
		          		currentName = columnHeader.text();
		          		
		          		if ( columnHeader.hasClass('no-select-filter') ) return true;

		          var selectbox = $('<div class="select-box"><select><option value="">'+ currentName +'</option></select></div>').appendTo( $(column.header()).empty() );

		              $('.dataTables_wrapper thead').find('select')
		              .on( 'change', function () {
		                  var val = $(this).val();
		                  $('#search-dt').val(val).keyup().focus();		                  
		              } );

			          // Percorre todas as colunas pegando apenas valores únicos
			          column.data().unique().sort().each( function ( d, j ) {
			            var element = $(d);
			            // Se for passado algum elemento .filter o texto sugerido é o que vai pro select
			            if (element.hasClass('filter')) {
			            	d = element.text();            
			            }
			            $(column.header()).find('select').append( '<option value="'+d+'">'+d+'</option>' )
			          });          

        });
      }
		});		

		
		$('#search-dt').keyup(function(){		   
		   dt.search($(this).val()).draw() ;
		});
		$('.js-toggle-search').on('click', function(e){
			$('.search-wrapper').toggleClass('active');
			$('#search-el-top').focus();
		});
		$('[data-toggle="tooltip"]').tooltip(); 

		/* Formulários ACF */
		/* ----------------------------------------- */
			// input file
			var form = document.getElementById('acf-form');
			if (form) {

				// Verifica se existe algum item de anexo
				// É necessário colocar a class .acf-anexo ao criar o campo
				var container = $('.acf-field.acf-anexo'),
						// Salva o valor da descrição para ser usado como placeholder
						placeholder = container.find('.description').html();

				
			  container.find(".hide-if-value" )
			  	.append('<input type="text" placeholder="'+ placeholder +'" class="nome-arquivo">')
			  	.append('<label class="btn btn-secondary mb-0" for="acf[field_596cf68eea2ff]">Procurar</label>');			  				  
			  
			  var fileInput  = document.querySelector( ".acf-anexo input[type='hidden']" ),  
			      button     = document.querySelector( ".btn" ),
			      the_return = document.querySelector(".nome-arquivo");
				  
				  button.addEventListener( "click", function( event ) {
				    fileInput.focus();
				    return false;
				  });  

				  fileInput.addEventListener( "change", function( event ) {  
				    the_return.value = this.value;  
				  });  
			};
		/* ----------------------------------------- Formulários ACF */		

		/* Formulário UPload */
		/* ----------------------------------------- */
			/*Brought click function of fileupload button when text field is clicked*/
			$("#uploadtextfield").click(function() {
				$('#fileuploadfield').click()
			});

			/*Brought click function of fileupload button when browse button is clicked*/
			$("#uploadbrowsebutton").click(function() {
				$('#fileuploadfield').click()
			});

			/*To bring the selected file value in text field*/	
			$('#fileuploadfield').change(function() {
				$('#uploadtextfield').val($(this).val());
			});	
		/* ----------------------------------------- Formulário UPload */
		

		/*  Default Scripts */
		/* ----------------------------------------- */			
		// $('.sidebar__inner').affix({
	 //    offset:{
  //       top: $('#header').outerHeight(),
  //       bottom: $('footer#footer').outerHeight() + 50
	 //    }
		// });
			
		// Mascara de DDD e 9 dígitos para telefones
		var SPMaskBehavior = function (val) {
			  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
			}, spOptions = { onKeyPress: function(val, e, field, options) { field.mask(SPMaskBehavior.apply({}, arguments), options); } };
		$('.mask-phone, input[type="tel"], .acf-tel input[type="text"]')
			.mask(SPMaskBehavior, spOptions);

		$('.acf-money input[type="text"]').mask('000.000.000.000.000,00', {reverse: true});
		$('.acf-cnpj input[type="text"], .mask-cnpj').mask('00.000.000/0000-00', {reverse: true});
		$('.acf-cpf input[type="text"], .mask-cpf').mask('000.000.000-00', {reverse: true});
		$('.acf-nascimento input[type="text"], .mask-date').mask('00/00/0000', {reverse: true});

		// SELECT , caso queira excluir algum elemento, colocar 'select:not(elementos)'
		$('select').not('.multiple').wrap('<div class="select-box"></div>');

		// Fancybox
		$("a[href$='.jpg'], a[href$='.png'], a[href$='.jpeg'], a[href$='.gif'], .fancybox").not('.no-fancy').fancybox({			
			loop : false,
		});  	
		
		window.onscroll = function() {
			scrollFunction();
		};
				
		function scrollFunction(){
			if (document.body.scrollTop > 40 || document.documentElement.scrollTop > 40) {
				document.getElementById('scroll-top').style.opacity = 1
			} else {
				document.getElementById('scroll-top').style.opacity = 0
			}
		}		

		// Rolagem suave
		$('a.smoothscroll').click(function() {
		  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		    var target = $(this.hash); target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		    if (target.length) { $('html,body').animate({ scrollTop: target.offset().top }, 1000); return false; }
		  }
		});  
		/* -----------------------------------------  Default Scripts */	
	});
})(jQuery, this);