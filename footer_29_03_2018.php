<?php $homeID = get_option( 'page_on_front' ); ?>
	<footer id="footer" role="contentinfo">		
		<div id="newsletter">
			<div class="container">
				<h3>RECEBA NOSSAS NOVIDADES</h3>
				<?php echo do_shortcode('[contact-form-7 id="109" title="Receba nossas novidades" html_class="form-inline justify-content-center align-items-start"]') ?>
			</div>
		</div>
		<div id="infos" class="container pb-4">			
			<div class="row">
				<div class="col-12 col-sm-6 col-lg-3 order-11 order-lg-1">
					<a class="logotipo logotipo-rodape" href="http://secovis.com.br/" target="_blank" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php echo get_field('logotipo_rodape', $homeID); ?>" class="img-fluid" alt="Secovis Brasil"/>
					</a>
				</div>
				<div class="col-12  col-sm-4 col-lg-3 order-1">
					<?php 
						echo '<h4>Acesse também:</h4>';
					  wp_nav_menu( array(         
					    'theme_location'    => 'local',
					    'depth'             => 2,
					    'container'         => 'div',
					    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
					    'walker'            => new WP_Bootstrap_Navwalker()
					  )); 
					?>      
				</div>
				<div class="col-12  col-sm-4 col-lg-3 order-1">
					<h4>ONDE ESTAMOS</h4>
					<h4>SEDE</h4>
					<p class="endereco_info" style="margin-bottom: 0px;"><?php echo get_field('endereco_info',get_option( 'page_on_front' ));?></br></p>
					<h4>REGIONAIS</h4>
					<p class="endereco_info" style="margin-bottom: 0px;"><a href="<?php echo home_url() ?>/quem-somos/regionais/" style="margin-top: -10px;">Clique aqui e conheça nossa abrangência</a></p>
				</div>
				<div class="col-12  col-sm-4 col-lg-2 order-1">
					<h4>Telefone:</h4>
					<?php
						$tels = get_field('telefone_info', $homeID);
						if ($tels) {											
							foreach ($tels as $tel) {
								$fullNumber = $tel['ddd'] . $tel['numero'];
								$icone = $tel['icone'] ? $tel['icone'] : '';								
								
								echo 	'<a class="telefone_info"  title="Ligue para nós!" href="tel:'.$fullNumber.'">',
												'<small>'.$tel['ddd'].'</small>'.$tel['numero'],
												$icone,
											'</a><br>';
							}
						}
					?>
				</div>
				<div class="col-12  col-sm-6 col-lg-1  order-12">
					<a id="react-logo" title="Design por React" href="https://www.react.ag/?source=<?php echo sanitize_title(get_bloginfo('name')); ?>" target="_blank"><img src="<?php images_url('logo_react.png') ?>" alt="React" class="img-fluid"></a>
				</div>
			</div>
			
		</div>		
	</footer>
	
	<a href="#header" class="smoothscroll" id="scroll-top"><i class="fa fa-angle-up fa-fw"></i></a>
<?php wp_footer(); ?>

<!--Start of Zendesk Chat Script-->
	<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
	$.src="https://v2.zopim.com/?3lsnpbl7qwGYxKwpA3dNlouEngUUtaYE";z.t=+new Date;$.
	type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
	</script>
<!--End of Zendesk Chat Script-->

</body>
</html>
