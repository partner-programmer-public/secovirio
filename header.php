<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />

		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?v=2.0" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link href="<?php echo get_template_directory_uri()	?>/assets/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


		<!-- google maps -->
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1P-H_fyEh6IaGS_mdIAPnMUIiQhKON2s"></script>
		<?php
			if ( is_singular() && get_option( 'thread_comments' ) )
				wp_enqueue_script( 'comment-reply' );
			wp_head();
		?>
	
		<script> 
			// Browser update
			var $buoop = {vs:{i:10,f:-4,o:-4,s:8,c:-4},unsecure:true,api:4}; 
			function $buo_f(){ 
			 var e = document.createElement("script"); 
			 e.src = "//browser-update.org/update.min.js"; 
			 document.body.appendChild(e);
			};
			try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
			catch(e){window.attachEvent("onload", $buo_f)}
		</script>			
 

     <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-28841841-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-28841841-1');
</script>

		<script src='https://www.google.com/recaptcha/api.js'></script>

	</head>

	<body id="main-content" <?php body_class(); ?>>
		
		<?php $mobileLogo = get_field('mobile_logotipo', get_option( 'page_on_front' )); ?>
		<header id="header" class="pb-2 pb-xl-0">			
			<div class="container-fluid">
				<div class="row">
					<div class="d-none d-xl-block col-xl-3">
						<a class="navbar-brand" href="<?php echo get_home_url(); ?>">     
						  <img src="<?php echo get_field('logotipo', get_option( 'page_on_front' )); ?>" class="img-fluid"/>    
						  <span class="sr-only"><?php echo get_bloginfo( 'name' ) ?></span>
						</a>
					</div>
					<div class="d-flex flex-column-reverse col-xl-9 flex-lg-column">

						<div class="d-none d-lg-flex flex-column flex-sm-row" id="headline-menu-wrapper">
							
							<?php if( have_rows('sociais_info',get_option( 'page_on_front' )) ): ?>
								<ul class="redes-sociais mb-2 mb-lg-0 list-inline d-flex justify-content-center align-items-center">
									<?php  while ( have_rows('sociais_info',get_option( 'page_on_front' )) ) : the_row(); ?>
										<li class="list-inline-item">
											<a href="<?php the_sub_field('url_info'); ?>" target="_blank">
												<?php the_sub_field('icone_info'); ?>
											</a>
										</li>
									<?php endwhile; ?>
								</ul>
							<?php endif; ?>

							<?php if( have_rows('links_extras',get_option( 'page_on_front' )) ): ?>

								<ul class="botoes-links-extras">

									<?php  while ( have_rows('links_extras',get_option( 'page_on_front' )) ) : the_row(); ?>

										<li class="">
											<?php $link = get_sub_field('link'); if( $link ): 
													$text = get_sub_field( 'imagem' ) ? '<img src="'.get_sub_field( 'imagem' ).'" alt="'.$link['title'].'">' : $link['title']; 
											?>
											  <a class="btn <?php the_sub_field( 'estilo_botao' ); ?>" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $text; ?></a>
											<?php endif; ?>
										</li>

									<?php endwhile; ?>

									<?php if (is_user_logged_in()): ?>
										<li>
											<a href="<?php echo wp_logout_url(get_home_url()) ?>" class="btn btn-dark">DESLOGAR</a>
										</li>
									<?php endif ?>

								</ul>

							<?php endif; ?>

						</div>
						<?php 
							get_partial('_menu-nav'); 
							get_partial('_search-wrapper'); 
						?>
					</div>
				</div>		
			</div>
		</header>



