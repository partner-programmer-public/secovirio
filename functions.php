<?php

// Setup Framework Base Functions
require_once ('functions/setup.php');

// Functions to help
require_once ('functions/custom-helpers.php');

// Filters, Hooks and functions about wp-admin
require_once ('functions/wp-admin.php');

// Wordpress Bootrap Navwalker
require_once ('functions/vendor/wp_bootstrap_navwalker.php');

// Custom functions for theming
require_once ('functions/custom-functions.php');
require_once ('functions/custom-acf.php');
require_once ('functions/custom-wpcf7.php');


require_once ('functions/custom-shortcakes.php'); 



/* CPT Downloads */
/* ----------------------------------------- */
   add_filter('add_meta_boxes', 'disable_download_meta_box');
   function disable_download_meta_box() {

    // Remove metas em Downloads
    remove_meta_box( 'tagsdiv-cat_downloads', 'download', 'side');
   }
/* ----------------------------------------- CPT Downloads */    


function adjust_single_breadcrumb( $link_output) {
	if(strpos( $link_output, 'breadcrumb_last' ) !== false ) {
		$link_output = '';
	}
   	return $link_output;
}
add_filter('wpseo_breadcrumb_single_link', 'adjust_single_breadcrumb' );


/** Add numero da pagina ao titulo e Meta Descricao SEO **/
if ( ! function_exists( 'multipage_metadesc' ) ){
   function multipage_metadesc( $s ){
      global $page;
      $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
      ! empty ( $page ) && 1 < $page && $paged = $page;
      $paged > 1 && $s .= ' - ' . sprintf( __( 'Página %s' ), $paged );
      return $s;
   }
   add_filter( 'wpseo_metadesc', 'multipage_metadesc', 100, 1 );
}

if ( ! function_exists( 'multipage_title' ) ){
   function multipage_title( $title ){
      global $page;
      $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
      ! empty ( $page ) && 1 < $page && $paged = $page;
      $paged > 1 && $title .= ' - ' . sprintf( __( 'Página %s' ), $paged );
      return $title;
   }
   add_filter( 'wpseo_title', 'multipage_title', 100, 1 );
}


add_action('pre_get_posts', 'filter_press_tax');

function filter_press_tax( $query ){
    if( $query->is_main_query() && $query->is_tax('tipo')):
        $query->set('posts_per_page', 16);
        return;
    endif;
}

function template_chooser($template)   
{    
  global $wp_query;   
  $post_type = get_query_var('post_type');   
  if( $wp_query->is_search && $post_type == 'classificado' )   
  {
    return locate_template('search-classificado.php');  //  redirect to archive-search.php
  }   
  return $template;   
}
add_filter('template_include', 'template_chooser');   

?>


