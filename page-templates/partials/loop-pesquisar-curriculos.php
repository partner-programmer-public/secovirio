<?php 	
	// Se for administrador ou "membro" exibe os curriculus
	if (current_user_can('manage_options') || is_membro()) {		
?>
	<header>
	  	<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>
		<h1 class="page-title color-seg h1"> <?php the_title(); ?> </h1>
	</header>			

	<?php 
		// Busca um curriculum especifico baseado no GET
		if (isset($_GET['curriculum'])) {
			
			$userID = $_GET['curriculum'];
			$user = get_user_by( 'id', $userID );

			if (!$user) { echo '<h2>Curriculum não encontrado.</h2>'; } 
			else {

				$fields = [
					'Nome:' => 'nome_user',
					'E-mail:' => 'e-mail_user',
					'Telefone:' => 'telefone_user',
					'CPF:' => 'cpf_user',
					'Nascimento:' => 'nascimento_user',


					'Cidade:' => 'cidade_user',
					'Estado:' => 'estado_user',

					'Escolaridade:' => 'escolaridade_user',
					'Área:' => 'area_user',
					'Função:' => 'funcao_user',
					
					'Experiência profissional:' => 'experiencia_profissional',
					'Objetivos:' => 'objetivos',
					
					'Anexo:' => 'anexo',
										
				];				
				
				echo '<br><br>';

				foreach ($fields as $label => $key) {
					$value = get_user_meta($user->ID, $key, 1);
					$value = $key == 'anexo' ? '<a href="'.wp_get_attachment_url($value).'" download target="_blank" class="btn btn-xs btn-rosaescuro text-white ml-3">Baixar Anexo</a>' : $value;
					$output = $value ? $value : '-';					
						
						echo 	'<p>',
										'<strong>'.$label.'</strong> ',
										$output,
									'</p>';					
				} // foreach ($fields		

			} //if ($user)

			echo '<br><a href="'.get_permalink().'" class="btn btn-secondary">Voltar a pesquisa</a>';

		// Se não for enviado nenhum ID especifico,
		// exibe a tabela para busca
		} else { ?>

			<div class="row table-secovi-row">
				<div class="container-fluid">
					<div id="search-box" class="search-form d-sm-flex">
						<input type="text" class="form-control search-field icon-search-right" name="search-table" id="search-dt">
						<button type="submit" class="btn btn-rosaclaro d-inline-block invisible">BUSCAR</button>
					</div>
				</div>
				<table class="table-sm table-secovi" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th class="no-order no-select-filter">Nome</th>
							<th width="220" class="no-order">Área</th>
							<th width="90" class="no-order">Função</th>
						</tr>
					</thead>		        	
					<tbody>
						<?php 
						
						$args = [
								'role' => 'Subscriber',
								'meta_query' => [
									'RELATION' => 'AND',
									[
										'key' => 'user_type',
										'value' => 'cadastrar-curriculo'
									]
								]
							];
						$curriculosQuery = new WP_User_Query( $args );
						$curriculos = $curriculosQuery->get_results();
						
						if ($curriculos) {
							foreach ($curriculos as $user) {
								// echo '<pre>'. print_r($user, 1) . '</pre>';
								$col1 = get_user_meta( $user->ID,'nome_user', 1);
								$col2 = get_user_meta( $user->ID,'area_user', 1);
								$col3 = get_user_meta( $user->ID,'funcao_user', 1);
								echo   '<tr data-url="'.get_permalink().'?curriculum='.$user->ID.'">',
													'<td>'.$col1.'</td>',
													'<td>'.$col2.'</td>',
													'<td>'.$col3.'</td>',
												'</tr>';
							}
	  					}		        				
						wp_reset_postdata();
				?>
			</tbody>
		</table>
	</div>

		<?php } 
	} else { 
		echo '<h2>Página disponível apenas para administradores.</h2>';
		echo '<p>Se você é um administrador, faça o <a href="'.wp_login_url().'">login</a>.</p>';
	}
 ?>
