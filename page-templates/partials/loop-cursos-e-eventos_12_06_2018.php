<?php 
	$today = date('Ymd');
	$cpts = new WP_Query( [
		'post_type' => 'agenda', 'posts_per_page' => -1,
		'meta_key'	=> 'data',
		'orderby'	=> 'meta_value_num',
		'order'		=> 'ASC',
		'meta_query' => array(								
		    array(
	        'key'		=> 'data',
	        'compare'	=> '>=',
	        'value'		=> $today,
		    )
	    ),
	] );
	$meses = [];
	while ( $cpts->have_posts() ) : $cpts->the_post();
		// get raw date
		$data = get_field('data', false, false);
		 // make date object
		$data = new DateTime($data);
		$meses[] = date_i18n("m/Y", $data->getTimestamp());
	endwhile; 		
	$meses = array_unique($meses);	
?>
<header>
  <?php
    if ( function_exists('yoast_breadcrumb') ) {
    	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
    }
  ?>
	<h1 class="page-title color-seg h1">
		<?php post_type_archive_title(); ?>
	</h1>
				
</header>			

<div class="row table-secovi-row">
	<div class="container-fluid">
		<div id="search-box" class="search-form d-sm-flex">
			<input type="text" class="form-control search-field icon-search-right" name="search-table" id="search-dt">
			<button type="submit" class="btn btn-rosaclaro d-inline-block invisible">BUSCAR</button>
		</div>
	</div>
	<table class="table-sm table-secovi" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th width="140" class="no-order no-select-filter">
					<div class="select-box">
						<select>
							<option value="">Data</option>
							<?php 
								array_map(function($mes){ 
									echo '<option value="'.$mes.'">'.$mes.'</option>'; 
								}, $meses); 
							?>
						</select>
					</div>
				</th>
				<th class="no-order no-select-filter">Título</th>
				<th width="150" class="no-order">Classificação</th>
			</tr>
		</thead>		        	
		<tbody>
			
			<tr data-url="https://www.secovirio.com.br/agenda/administracao-de-condominios/" role="row" class="odd"><td><span class="filter d-none">junho</span>04/06/2018</td><td>Administração de Condomínios</td><td>Cursos Centro</td></tr>

			<?php 					
					while ( $cpts->have_posts() ) : $cpts->the_post();
					 $classificacao = wp_get_post_terms(get_the_id(), 'classificacao', array("fields" => "names"));					 
					 // get raw date
					 $data = get_field('data', false, false);
					 // make date object
					 $data = new DateTime($data);

					 echo   '<tr data-url="'.get_permalink( get_the_id() ).'">',
					 					'<td>',
					 						'<span class="filter d-none">'.date_i18n("F", $data->getTimestamp()).'</span>',
					 						get_field( 'data' ),						
					 					'</td>',
					 					'<td>'.get_the_title().'</td>',
					 					'<td>'.(is_array($classificacao) ? implode(',', $classificacao) : '').'</td>',
					 				'</tr>';
					endwhile; 		        				
					wp_reset_postdata();
			?>
		</tbody>
	</table>
</div>