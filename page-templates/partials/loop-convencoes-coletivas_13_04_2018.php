<header>
  <?php
    if ( function_exists('yoast_breadcrumb') ) {
    	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
    }
  ?>
	<h1 class="page-title color-seg h1">
		<?php the_title(); ?>
	</h1>
				
	<p>		
		<?php the_content(); ?>
	</p>

	<br>

	<h2> Convenções Coletivas 	</h2>


</header>			



<div class="row table-secovi-row">
	<div class="container-fluid">
		<div id="search-box" class="search-form d-sm-flex">
			<input type="text" class="form-control search-field icon-search-right" name="search-table" id="search-dt">
			<button type="submit" class="btn btn-rosaclaro d-inline-block invisible">BUSCAR</button>
		</div>
	</div>
	<table class="table-sm table-secovi" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th width="130" class="no-order">Segmento</th>
				<th class="no-order">Localidade</th>
				<th width="90" class="no-order">Ano</th>
			</tr>
		</thead>		        	
		<tbody>
			<?php 
					$cpts = new WP_Query( [
						'post_type' => 'convencao_coletiva', 'posts_per_page' => -1,
						'orderby' => 'meta_value', 'meta_key' => 'ano'
					] ); 
					// echo '<pre>'. print_r($cpts, 1) . '</pre>';
					
					while ( $cpts->have_posts() ) : $cpts->the_post();
					 $localidades = wp_get_post_terms(get_the_id(), 'localidade', array("fields" => "names"));
					 $segmentos = wp_get_post_terms(get_the_id(), 'segmento', array("fields" => "names"));
					 $ano = wp_get_post_terms(get_the_id(), 'ano', array("fields" => "names"));
					 // $nomes =  wp_get_post_terms(get_the_id(), , array("fields" => "names"));
					 // echo '<pre>'. print_r($ano, 1) . '</pre>';
					 $help = get_field( 'help' ) ? 'data-toggle="tooltip" title="'.get_field('help').'"' : '';
					 echo   '<tr data-url="'.get_permalink( get_the_id() ).'">',
					 					'<td>'.(is_array($segmentos) ? implode(',', $segmentos) : '').'</td>',
					 					// '<td '.$help.' >'.$post->title.'</td>',
					 					the_title( '<td '.$help.' >', '</td>' ),
					 					'<td>'.(is_array($ano) ? implode(',', $ano) : '').'</td>',
					 				'</tr>';
					endwhile; 		        				
					wp_reset_postdata();
			?>
		</tbody>
	</table>
</div>