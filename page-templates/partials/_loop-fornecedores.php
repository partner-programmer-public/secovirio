<br>
<form action="" class="acf-form form-cadastro">
	<h5 class="text-primary">Filtrar por:</h5>
	<div class="row pt-2">
		<div class="col-md-6">
			<input type="text" class="form-control bg-white" name="username" placeholder="NOME:">
		</div>
		<div class="col-md-6">
			<select name="categoria">
				<option value="">CATEGORIA: TODAS</option>				
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<select name="cidade_user">
				<option value="">CIDADE: TODAS</option>				
			</select>
		</div>
		<div class="col-md-6">
			<select name="bairro_user">
				<option value="">BAIRRO: TODAS</option>				
			</select>
		</div>
	</div>
	<input type="submit" class="btn btn-rosaclaro w-100 mb-0" value="BUSCAR">
</form>

<?php if (get_field( 'banner' )): ?>
	<br>
	<img src="<?php the_field( 'banner' ); ?>" class="img-fluid mx-auto d-block">
<?php endif ?>

<div class="list-fornecedores">
	<?php 
		$args = [
			'role' => 'Subscriber',
			'meta_query' => [
				'RELATION' => 'AND',
				[
					'key' => 'user_type',
					'value' => 'cadastro-fornecedor'
				]
			]
		];
		$fornecedoresQuery = new WP_User_Query( $args );
		$fornecedores = $fornecedoresQuery->get_results();
		// echo '<pre>'. print_r($fornecedoresQuery, 1) . '</pre>';
		
		if ($fornecedores) {
			foreach ($fornecedores as $user) { 
				$avatarID = get_field( 'logo_user', 'user_'. $user->ID );
				$imgSrc = wp_get_attachment_image_src( $avatarID, 'thumbnail');
				// echo '<pre>'. print_r($imgSrc, 1) . '</pre>';
				
		?>
			<div class="fornecedor d-flex align-items-start">				
				<?php if ($avatarID): ?>
					<div class="box-avatar">
						<img src="<?php echo $imgSrc[0]; ?>" alt="Logotipo Empresa">
					</div>
				<?php endif ?>				
				<div class="box-infos">
					<h2 class="text-primary h5"><?php echo get_field( 'empresa_user', 'user_'. $user->ID ); ?></h2>
					<p class="small font-weight-bold mb-0">
						<?php 
								if (get_field( 'categoria_user', 'user_'. $user->ID )) {									
									echo get_field( 'categoria_user', 'user_'. $user->ID ) . '<br>';
								}
								if (get_field( 'cidade_user', 'user_'. $user->ID )) {									
									echo get_field( 'cidade_user', 'user_'. $user->ID ) . '<br>';
								}
								if (get_field( 'bairro_user', 'user_'. $user->ID )) {									
									echo get_field( 'bairro_user', 'user_'. $user->ID );
								}
						 ?>
					</p>
				</div>
				<div class="box-actions">
					<a href="<?php echo get_author_posts_url($user->ID) ?>" class="btn btn-rosaclaro">
						+ INFORMAÇÕES
					</a>
					<div class="text-center">
						<a href="<?php echo get_author_posts_url($user->ID) ?>#recomendacoes" class="link-recomendacao"><?php echo get_recomendacoes($user->ID); ?></a>
					</div>
				</div>
			</div>
		<?php 
			}
		}
	?>
</div>

<?php
	$links = get_field( 'links_rodape' );	
	if ($links) {
		echo '<ul class="links-rodape list-inline text-right">';
		foreach ($links as $link) { $link = $link['link'];
			// $link['title'] = $link['title'] == 'Atualizar Cadastro' && !is_user_logged_in() ? 'Cadastro Fornecedor' : $link['title'];
			echo 	'<li class="list-inline-item">',
							'<a class="" title="'.$link['title'].'" href="'.$link['url'].'" target="'.$link['target'].'">',
								$link['title'],
							'</a>',
						'</li>';
		}	
		echo '</ul>';
	}	
	
?>