		
		<?php if ( have_rows( 'categorias' ) ) : ?>
			
			<form id="search-box" role="search" method="get" class="search-form d-sm-flex" action="">
				<select class="downloads-w-filter bg-white" name="categoria">
					<option value="">CATEGORIA: TODAS</option>
					<?php 
						$items = get_field( 'categorias' ); 
						$current = isset($_GET['categoria']) ? $_GET['categoria'] : 0 ;																	
						for ($i=0; $i < count($items); $i++) { 
							$title = $items[$i]['titulo_da_categoria'];
							$val = sanitize_title($title);
							echo '<option '. ($val === $current ? 'selected' : '') .' value="'. $val .'">'.$title.'</option>';
						}											
					?>
				</select>	
				<button type="submit" class="btn btn-rosaclaro d-inline-block mb-0">FILTRAR</button> 
			</form>
			
			<div class="itens-download">
				<br><br>
				<?php
					// echo '<pre>'. print_r($items, 1) . '</pre>';
					
					// Se for passado uma categoria
					if ($current) {
						$listItems = array_filter($items, function($item) use($current){
							// echo '<pre>'. print_r($item, 1) . '</pre>';
							return sanitize_title($item['titulo_da_categoria']) == $current;
						});
						$listItems = array_merge($listItems);
						$listItems = $listItems[0]['itens'];
						
					// Se não estiver selecionado nenhuma categoria
					// Remove o título das categorias e deixa apenas os itens
					} else {
						$listItems = array_reduce(array_column($items, 'itens'), 'array_merge', []);
					}

					/* Páginação ACF */
					/* ----------------------------------------- */
						$page = get_query_var( 'page' ) ? get_query_var('page') : 1;

						// Variables
						$row              = 0;
						$items_per_page  = 6; // How many images to display on each page										
						$total            = count( $listItems );
						$pages            = ceil( $total / $items_per_page );
						$min              = ( ( $page * $items_per_page ) - $items_per_page ) + 1;
						$max              = ( $min + $items_per_page ) - 1;
						
					/* ----------------------------------------- Páginação ACF */		
					// echo '<pre>'. print_r($listItems, 1) . '</pre>';
					// die();
					echo '<div class="row">';

						foreach ($listItems as $item) { 							
							
							
							$row++;
							// Ignore this image if $row is lower than $min
							if($row < $min) { continue; }
							// Stop loop completely if $row is higher than $max
							if($row > $max) { break; }
							
							$href = $item['tipo'] == 'url_externa' ? $item['url'] : $item['arquivo'];
							$imgSrc = $item['capa']['sizes']['thumb-download'];
							echo 	'<div class="col-sm-6 col-lg-4">',
											'<a href="'.$href.'" target="_blank" title="Saiba mais">',
												'<img class="img-fluid" src="'.$imgSrc.'">',
												'<p class="card-text">'.$item['titulo'].'</p>',
											'</a>',
										'</div>';

							
						}; 
					echo '</div>';
					?>
			</div>


			<div class="row pagination text-right">
        <?php 
            // Pagination
              echo paginate_links( array(
                'base' => get_permalink() . '%#%' . '/',
                'format' => '?page=%#%',
                'current' => $page,
                'total' => $pages,
                // 'type' => 'list',
                'next_text' => '<i class="fa fa-arrow-right"></i>',
                'prev_text' => '<i class="fa fa-arrow-left"></i>',
              ) );
         ?>               
			</div>

		<?php else: ?>
			<h3>Ainda não temos nenhum item cadastrado <br> Volte em breve :)</h3>
			<br><br>
		<?php endif; ?>