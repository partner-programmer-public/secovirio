<?php 
	global $wp_query; 
	$obj = get_post_type_object($wp_query->get('post_type'));
?>
<header>
	<?php
	  if ( function_exists('yoast_breadcrumb') ) {
	  	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
	  }
	?>
	<h1 class="page-title color-seg h1">	
		Busca
	</h1>
	<?php get_search_form(); ?>		    			
</header>			

<?php 		
	
		echo get_partial('_header-search');
		echo get_partial('_header-archive');

		if (is_post_type_archive('legislacao')) {		
			// echo '<pre>'. print_r($obj, 1) . '</pre>';
				
			$args = [
				'taxonomy' => 'cat_legislacao',
				'title_li' => ''
			];
			echo 	'<ul class="list-taxonomy list-unstyled">',
							wp_list_categories($args);
						'</ul>';
			
		} else {
						
			if (have_posts()): while (have_posts()) : the_post();
				global $post;
				 switch ($post->post_type) {
				 	case 'download':
				 		 $post_type = 'download';
				 		break;
				 	
				 	default:
				 		$post_type = 'blog';
				 		break;
				 }				
				
				get_partial('_loop-'.$post_type);
				
			endwhile; 
				if (function_exists('wp_pagenavi')) { 
					echo '<div class="clearfix"></div>';
					wp_pagenavi();
				};				
			endif; 
			wp_reset_query();

		}
	
	 

?>
