<div class="search search-wrapper">
  <div class="container">
    <div class="search-wrapperform">
      <form id="search-box" role="search" method="get" class="search-form d-flex" action="<?php echo home_url( '/' ); ?>">
        <label class="sr-only"> Buscar no site </label>        
        <input id="search-el-top" type="search" required class="search-field form-control icon-search-right" placeholder="Digite sua busca..." value="" name="s" title="BUSCA" />
        <button type="submit" class="btn btn-rosaclaro d-inline-block">BUSCAR</button>        
        <i class="fa fa-times js-toggle-search"></i>
      </form>
    </div>
  </div>
</div>