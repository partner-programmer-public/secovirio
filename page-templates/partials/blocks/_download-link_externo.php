<?php 
	if (!$item) return; 
	$title = $item['url']['title'];
	$href = $item['url']['url'];
	$target = $item['url']['target'];
	$descricao = $item['descricao']
?>

<a href="<?php echo $href ?>" title="Saiba mais sobre <?php echo $title ?>" target="<?php echo $target ?>" class="link-download">
	<p><strong><?php echo $title ?></strong></p>
	<p><?php echo $descricao ?></p>
	<span class="btn btn-rosaclaro">BAIXAR</span>
</a>