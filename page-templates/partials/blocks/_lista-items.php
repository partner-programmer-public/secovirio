<?php 
  $items = get_field( 'items' );
  if ($items) {
    foreach ($items as $item) { ?>
      <br><br>
      <p class="text-primary" id="<?php echo sanitize_title($item['titulo']); ?>">
        <strong><?php echo $item['titulo']; ?></strong>
      </p>
      <div class="d-flex lista-items">            
         <?php if ($item['imagem']): ?>
            <div class="imagem">
              <img src="<?php echo $item['imagem']; ?>" class="img-fluid" alt="Logo <?php echo $item['titulo']; ?>">  
            </div>
         <?php endif ?>
         <div>
           <?php echo $item['descricao']; ?>
         </div>         
      </div>
  <?php
    }
  }
?>