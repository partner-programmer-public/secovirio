<div class="container">
  <!-- banner -->
  <?php 
    $home_page_id = get_option( 'page_on_front' );
    $classificados_page_id = get_field( 'pagina_classificados', $home_page_id);
    $banner = get_field('banner_classificados', $classificados_page_id);
    
    if($banner):
      extract($banner);

      $image = get_sub_field('imagem_banner');     
    ?>
      <div class="ads-banner-wrapper" style="background-color:<?php echo $cor_de_fundo_banner?>;">
        <div class="row">
          <div class="col-md-7 texts-wrapper">
            <h4><?php echo $titulo_banner; ?></h4>
            <p><?php echo $descricao_banner; ?></p>
            <a title="<?php echo $link_botao['title']; ?>" href="<?php echo $link_botao['url']; ?>"  target="<?php echo $link_botao['target']; ?>" style="background-color:<?php echo $cor_botao; ?>;" onmouseover="this.style.backgroundColor='<?php echo $cor_botao_hover;?>'" onmouseout="this.style.backgroundColor='<?php echo $cor_botao; ?>'">
              <?php echo $link_botao['title']; ?>
            </a>
          </div>
          <div class="col-md-5 image-wrapper">
            <img src="<?php echo $imagem_banner['url'] ?>" class="d-none d-sm-block img-fluid" alt="<?php echo $imagem_banner['alt'] ?>"/>
            <img src="<?php echo $imagem_banner_mobile['url'] ?>" class="d-block d-sm-none img-fluid" alt="<?php echo $imagem_banner_mobile['alt'] ?>"/>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <!-- banner end -->
</div>