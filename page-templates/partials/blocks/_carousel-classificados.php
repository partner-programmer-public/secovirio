﻿<!-- carousel-classificados -->
<?php
  global $post;
  $page_slug = $post->post_name;
  
  // echo '<pre>'.print_r(is_tax('tipo'),1). '</pre>';
  // die();
  if (is_tax('tipo')) :
    
    $anuncios_destaques_ids = get_field("selecao_destaques");
    // echo '<pre>'.print_r($anuncios_destaques_ids,1). '</pre>';
    // die();

    $query = new WP_Query(array(
      'post_type'       => 'classificado',
      'posts_per_page'  => 4,
      'post__in'        => $anuncios_destaques_ids,
      'post_status'     => 'any',
      'orderby'         => 'post__in',
    ));

    if (!$query) return;
    
    
    if ($query->have_posts()) {
      if ($page_slug == 'servicos') {
        $cat_formated = 'serviços';
      } else if ($page_slug == 'emprego') {
        $cat_formated = 'empregos';
      }
      
      echo  '<div class="carousel-classificados-wrapper">',
              '<h2 class="title">'.ucfirst(($cat_formated ? $cat_formated : $page_slug)).' em destaque</h2>',
              '<div class="container-fluid">';

                echo '<div class="slick slick-classificados">';
                  while ( $query->have_posts() ) : $query->the_post(); 	
                    global $post; ?>									
                      <article>
                        <a class="mx-auto d-block" href="<?php echo get_permalink(); ?>" title="<?php echo get_field('nome'); ?>">
                          <?php if (get_field('imagem')) : ?>
                            <div class="image-wrapper">
                              <img src="<?php echo get_field('imagem')['url']; ?>" alt="<?php echo get_field('imagem')['alt']; ?>">
                            </div>
                            <h4><?php echo get_field('nome'); ?></h4>
                            <p class="string_limit small">
                              <?php echo mb_strimwidth(get_field('descricao', false, false), 0, 62, "..."); ?>
                            </p>
                            <?php else: ?>
                            <h4><?php echo get_field('nome'); ?></h4>
                            <p class="string_limit medium">
                            <?php echo mb_strimwidth(get_field('descricao', false, false), 0, 187, "..."); ?>
                            </p>
                          <?php endif; ?>
                        </a>
                        <div class="texts-wrapper">
                          <p class="tel">(<?php echo get_field('telefone_ddd'); ?>) <?php echo get_field('telefone_num'); ?></p>
                          <?php if (get_field('whatsapp_num')) : ?>
                            <p class="wpp">(<?php echo get_field('whatsapp_ddd'); ?>) <?php echo get_field('whatsapp_num'); ?></p>
                          <?php endif; ?>
                          <p class="email"><?php echo get_field('email'); ?></p>
                          <?php if (get_field('site')) : ?>
                            <p class="site"><?php echo get_field('site'); ?></p>
                          <?php endif; ?>
                          <ul class="tags-list-wrapper">
                            <?php foreach($subcats_classificados = get_field('categoria_subcategoria') as $subcat) : ?>
                              <?php $terms = get_term_by('id', $subcat, 'tipo'); ?>
                              <li><?php echo $terms->slug; ?></li>
                            <?php endforeach; ?>
                          </ul>
                        </div>
                      </article>
                  <?php endwhile; 
                echo '</div>',
              '</div>',
          '</div>';
    }

    wp_reset_postdata();
    
    get_block('_classificados-banner');
  ?>

<?php else: ?>

  <?php

    $cats_classificados = ['produtos', 'servicos', 'emprego'];
    $index = 0;

    foreach($cats_classificados as $cat) {
      $ids = get_field("destaques_".$cat, false, false);

      $query = new WP_Query(array(
        'post_type'       => 'classificado',
        'posts_per_page'  => 4,
        'post__in'        => $ids,
        'post_status'     => 'any',
        'orderby'         => 'post__in',
      ));
    
      if (!$query) return;
      
      
      if ($query->have_posts()) {
        
        if ($cat == 'servicos') {
          $cat_formated = 'serviços';
        } else if ($cat == 'emprego') {
          $cat_formated = 'empregos';
        }
        echo  '<div class="carousel-classificados-wrapper">',
                '<h2 class="title">'.ucfirst(($cat_formated ? $cat_formated : $cat)).' em destaque</h2>',
                '<div class="container-fluid">';
    
                  echo '<div class="slick slick-classificados">';
                    while ( $query->have_posts() ) : $query->the_post(); 	
                      global $post; ?>									
                        <article>
                          <a class="mx-auto d-block" href="<?php echo get_permalink(); ?>" title="<?php echo get_field('nome'); ?>">
                            <?php if (get_field('imagem')) : ?>
                              <div class="image-wrapper">
                                <img src="<?php echo get_field('imagem')['url']; ?>" alt="<?php echo get_field('imagem')['alt']; ?>">
                              </div>
                              <h4><?php echo get_field('nome'); ?></h4>
                              <p class="string_limit small">
                                <?php echo mb_strimwidth(get_field('descricao', false, false), 0, 62, "..."); ?>
                              </p>
                              <?php else: ?>
                              <h4><?php echo get_field('nome'); ?></h4>
                              <p class="string_limit medium">
                              <?php echo mb_strimwidth(get_field('descricao', false, false), 0, 187, "..."); ?>
                              </p>
                            <?php endif; ?>
                          </a>
                          <div class="texts-wrapper">
                            <p class="tel">(<?php echo get_field('telefone_ddd'); ?>) <?php echo get_field('telefone_num'); ?></p>
                            <?php if (get_field('whatsapp_num')) : ?>
                              <p class="wpp">(<?php echo get_field('whatsapp_ddd'); ?>) <?php echo get_field('whatsapp_num'); ?></p>
                            <?php endif; ?>
                            <p class="email"><?php echo get_field('email'); ?></p>
                            <?php if (get_field('site')) : ?>
                              <p class="site"><?php echo get_field('site'); ?></p>
                            <?php endif; ?>
                            <ul class="tags-list-wrapper">
                              <?php foreach($subcats_classificados = get_field('categoria_subcategoria') as $subcat) : ?>
                                <?php $terms = get_term_by('id', $subcat, 'tipo'); ?>
                                <li><?php echo $terms->slug; ?></li>
                              <?php endforeach; ?>
                            </ul>
                          </div>
                        </article>
                    <?php endwhile; 
                  echo '</div>',
                  '<div class="btn-more-wrapper">',
              '<a title="Ver todos os '.($cat_formated ? $cat_formated : $cat).'" href="'.get_bloginfo( 'url' ).'/classificados/'.$cat.'">Ver todos os '.($cat_formated ? $cat_formated : $cat).'</a>',
              '</div>',
                '</div>',
            '</div>';
      }
    
      wp_reset_postdata();

      if ($index == 0) {
        get_block('_classificados-banner');
      }
      
      $index++;
    }

  ?>

<?php endif; ?>
<!-- carousel-classificados end -->