﻿<?php
	$lastPosts = get_field( 'ultimas_noticias' );
	
	if (!$lastPosts) return;

	$lastPosts = new WP_Query( [
		'post_type' => 'post', 'post__in' => $lastPosts,
		'orderby' => 'post__in'
	] ); 
		
		// Limita o tamanho do excerpt apenas nesse post
		add_filter( 'excerpt_length', function(){ return 30; } );
		if ($lastPosts->have_posts()) {
			echo  '<div id="carousel-last-posts-wrapper">',
							'<h2 class="title mb-3 mb-sm-0">Últimas Notícias</h2>',
							'<div class="container">';

								echo '<div class="slick slick-last-posts">';
									while ( $lastPosts->have_posts() ) : $lastPosts->the_post(); 	
										global $post;										
										echo 	'<article>',
														'<a class="mx-auto d-block" href="'.get_permalink().'" title="'.get_the_title().'">',
															'<h1>'.get_the_title().'</h1>',
															the_excerpt(),
															'<span class="btn btn-secondary d-inline-block">LEIA MAIS > </span>',
														'</a>',
													'</article>';
									endwhile; 
								echo '</div>',
							'</div>',
					'</div>';
		}

		wp_reset_postdata();

		// Restaura o padrão do tema
		add_filter( 'excerpt_length', 'twentyten_excerpt_length' );
?>