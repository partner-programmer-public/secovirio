<!-- banner -->
  <?php $homeID = get_option( 'page_on_front' ); ?>
  <?php if( have_rows('secoes_de_destaque',$homeID) ): ?>
    <div id="carousel-secoes-wrapper">
      <div class="container">
        <div class="slick slick-secoes">
            <?php while ( have_rows('secoes_de_destaque',$homeID) ) : the_row(); ?>
              <div>
                <div class="wrapper">
                  <img src="<?php the_sub_field( 'icone' ); ?>" class="img-fluid mx-auto d-block">
                  <h2 class="text-bodytext"> <?php the_sub_field( 'titulo' );?> </h2>
                  <?php 
                      echo '<p>'.get_sub_field( 'descricao' ).'</p>';
                      $link = get_sub_field('botao');
                      if( $link ): 
                  ?>
                      <br><a class="btn btn-secondary" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
                  <?php endif; ?>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        </div>
      </div>        
    </div>
  <?php endif; ?>
<!-- banner end -->