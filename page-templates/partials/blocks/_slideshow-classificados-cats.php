<?php
    $term = get_queried_object();
    $slider = get_field('slideshow_classificados_categorias', $term->taxonomy . '_' . $term->term_id);
    ?>

<?php if( $slider ): ?>
  <!-- slider -->
  <div id="slideshow" role="banner">
    <div class="banner">
      <?php 
        $image = $slider['imagem_slider'];
        $imageMobile = $slider['imagem_slider_mobile'];
        $url = $image['url'];
        $anchor = $slider['link_slider'];
        $alt = $image['alt'];
      
        $size = 'slider-destaque';
        $thumb = $image['url'];
      ?>
      <div>
        <?php if ($anchor): ?> <a href="<?php echo $anchor; ?>" title="Saiba mais"> <?php endif ?>
          <img src="<?php echo $url ?>" class="d-none d-sm-block img-fluid mx-auto" alt="<?php echo $alt ?>" style="min-width: 100%;"/>
          <img src="<?php echo $imageMobile['url'] ?>" class="d-block d-sm-none img-fluid mx-auto" alt="<?php echo $alt ?>"/>
        <?php if ($anchor): ?> </a> <?php endif ?>            
      </div>
    </div>
  </div>
  <!-- slider end -->
<?php endif; ?>