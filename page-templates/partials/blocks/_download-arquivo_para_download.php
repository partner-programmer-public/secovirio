<?php 
	if (!$item) return; 
	$capa = get_post($item['capa']);
	$arquivo = get_post($item['arquivo']);


	// echo '<pre>'. print_r($arquivo, 1) . '</pre>';
	
	$title = $arquivo->post_title;
	$href = $arquivo->guid;
	$target = '_blank';
	$descricao = $arquivo->post_excerpt;

?>

<a href="<?php echo $href ?>" title="Saiba mais sobre <?php echo $title ?>" target="<?php echo $target ?>" class="link-download <?php echo $item['acf_fc_layout'] ?>">
	<p><strong><?php echo $title ?></strong></p>
	<p>
		<?php if ($capa): ?>
			<img src="<?php echo $capa->guid; ?>" alt="icone para download" class="float-left">
		<?php endif ?>
		<?php echo $descricao ?>
	</p>
	<span class="btn btn-rosaclaro">BAIXAR</span>
</a>