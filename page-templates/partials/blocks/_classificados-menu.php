<?php
  global $post;

  $terms = get_terms( array( 'taxonomy' => 'tipo', 'parent' => 0, 'orderby' => 'id' ) );

  if ($terms):
  
  $isActiveTerm = get_queried_object();
  ?>
  <div class="main-categories-wrapper">
    <div class="container-fluid">
      <ul>
      <?php foreach ($terms as $term) : ?>
        <?php $icon = get_field('icone', $term->taxonomy . '_' . $term->term_id);?>
        <li><a 
            title="<?php echo $term->name ?>"
            href="<?php echo get_term_link($term);?>" 
            <?php if($term == $isActiveTerm){ echo 'class="active"';} ?> >
            <div class="icon"><img src="<?php echo $icon; ?>" alt="<?php $term->name; ?>"></div><?php echo $term->name ?>
          </a>
      </li>
      <?php endforeach; ?>
      </ul>
    </div>
  </div> 
<?php endif; ?>