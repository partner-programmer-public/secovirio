﻿<!-- revistas !-->
    
      <?php $homeID = get_option( 'page_on_front' ); ?>
      <?php if( have_rows('revistas',$homeID) ): ?>
        <div id="carousel-secoes-wrapper">
          <div class="container">
            <div class="slick slick-secoes">
                <?php while ( have_rows('revistas',$homeID) ) : the_row(); ?>
                  <div>
                    <div class="wrapper">
                      <img src="<?php the_sub_field( 'capa' ); ?>" class="img-fluid mx-auto d-block">
                      <h2 class="text-bodytext"> <?php the_sub_field( 'titulo' );?> </h2>
                      <?php 
                          $link = get_sub_field('url');
                          if( $link ): 
                      ?>
                          <br>
                      <?php endif; ?>
                    </div>
                  </div>
                <?php endwhile; ?>
              </div>
            </div>
          </div>        
        </div>
      <?php endif; ?>


    <!-- fim revistas !-->
?>