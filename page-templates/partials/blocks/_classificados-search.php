<?php 
  $object_queried = get_queried_object();
  $options = get_terms(['parent' => 0, 'taxonomy' => 'tipo', 'hide_empty' => false ]);
?>
<div class="search-classificados-wrapper">
  <div class="container-fluid">
    <form id="search-box" role="search" method="get" class="search-form d-flex" action="<?php echo home_url( '/' ); ?>">
      <div class="form-group">
        <label for="categorias">Categorias</label>
        <select class="downloads-w-filter bg-white" name="tipo" id="categorias">
          <option value="">todas</option>
          <?php 
            foreach ($options as $option):
              $sub_options = get_terms(['parent' => $option->term_id, 'taxonomy' => 'tipo', 'hide_empty' => false ]); 
              if ($sub_options) {
                echo '<optgroup label="'. $option->name .'">';
                  echo '<option value="'.$option->slug.'">Todos os '.$option->name.'</option>';
                  foreach ($sub_options as $sub_option ) {
                    echo '<option value="'.$sub_option->slug.'">'.$sub_option->name.'</option>';
                  }
                echo '</optgroup>';
              } else {
                echo '<optgroup label="'. $option->name .'">';
                  echo '<option value="'.$option->slug.'">Todos os '.$option->name.'</option>';
                echo '</optgroup>';
              }
            endforeach; 
          ?>
        </select>
      </div>
      <div class="form-group">
        <label for="search-field">Palavra-chave</label>
        <input id="search-field" type="search" class="search-field form-control" placeholder="ex: câmera de segurança"  name="s" title="Campo de busca" />
      </div>
      <input type="hidden" name="post_type" value="classificado" />
      <button type="submit" class="btn">Buscar</button>
    </form>
  </div>
</div>