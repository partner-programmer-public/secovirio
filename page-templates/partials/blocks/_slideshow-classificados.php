<!-- slider -->
  <?php if( have_rows('slider_classificados') ): ?>
    <div id="slideshow" role="banner">
      <div class="banner">
        <?php 
          while ( have_rows('slider_classificados') ) : the_row();
        
          $image = get_sub_field('imagem_slider');
          $imageMobile = get_sub_field('imagem_slider_mobile');
          $url = $image['url'];
          $anchor = get_sub_field('link_slider');
          $alt = $image['alt'];
        
          $size = 'slider-destaque';
          $thumb = $image['url'];
          // $thumb = $image['sizes'][ $size ];
        ?>
          <div>
            <?php if ($anchor): ?> <a href="<?php echo $anchor; ?>" title="Saiba mais"> <?php endif ?>
              <img src="<?php echo $url ?>" class="d-none d-sm-block img-fluid mx-auto" alt="<?php echo $alt ?>" style="min-width: 100%;"/>
              <img src="<?php echo $imageMobile['url'] ?>" class="d-block d-sm-none img-fluid mx-auto" alt="<?php echo $alt ?>"/>
            <?php if ($anchor): ?> </a> <?php endif ?>            
          </div>
        <?php endwhile; ?>
      </div>
    </div>
  <?php else: ?>
    <p>Nada cadastrado ainda.</p>
  <?php endif; ?>
<!-- slider end -->