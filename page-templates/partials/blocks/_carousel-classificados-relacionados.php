﻿<!-- carousel-classificados relacionados -->
<?php

    $aa = get_queried_object();

    var_dump($aa->ID);

    $category_detail = get_the_category($aa->ID);//$post->ID

    var_dump($category_detail);

    foreach($category_detail as $cd){
      var_dump($cd->cat_name);
    }

		$tax = single_term_title( '', false );
    
    $args = array(
      'post_type' => 'classificado',
      'posts_per_page'  => 4,
      'tax_query' => array(
        array(
          'taxonomy' => 'tipo',
          'field'    => 'slug',
          'terms' => $tax,
        ),
      ),
    );

    $query = new WP_Query( $args );

    if (!$query) return;
    
    
    if ($query->have_posts()) {
      if ($tax == 'servicos') {
        $cat_formated = 'serviços';
      } else if ($tax == 'emprego') {
        $cat_formated = 'empregos';
      }
      
      echo  '<div class="carousel-classificados-wrapper">',
              '<h2 class="title">'.ucfirst(($cat_formated ? $cat_formated : $tax)).' em destaque</h2>',
              '<div class="container-fluid">';

                echo '<div class="slick slick-classificados">';
                  while ( $query->have_posts() ) : $query->the_post(); 	
                    global $post; ?>									
                      <article>
                        <a class="mx-auto d-block" href="<?php echo get_permalink(); ?>" title="<?php echo get_field('nome'); ?>">
                          <?php if (get_field('imagem')) : ?>
                            <div class="image-wrapper">
                              <img src="<?php echo get_field('imagem')['url']; ?>" alt="<?php echo get_field('imagem')['alt']; ?>">
                            </div>
                            <h4><?php echo get_field('nome'); ?></h4>
                            <p class="string_limit small">
                              <?php echo mb_strimwidth(get_field('descricao', false, false), 0, 62, "..."); ?>
                            </p>
                            <?php else: ?>
                            <h4><?php echo get_field('nome'); ?></h4>
                            <p class="string_limit medium">
                            <?php echo mb_strimwidth(get_field('descricao', false, false), 0, 187, "..."); ?>
                            </p>
                          <?php endif; ?>
                        </a>
                        <div class="texts-wrapper">
                          <p class="tel">(<?php echo get_field('telefone_ddd'); ?>) <?php echo get_field('telefone_num'); ?></p>
                          <?php if (get_field('whatsapp_num')) : ?>
                            <p class="wpp">(<?php echo get_field('whatsapp_ddd'); ?>) <?php echo get_field('whatsapp_num'); ?></p>
                          <?php endif; ?>
                          <p class="email"><?php echo get_field('email'); ?></p>
                          <?php if (get_field('site')) : ?>
                            <p class="site"><?php echo get_field('site'); ?></p>
                          <?php endif; ?>
                          <ul class="tags-list-wrapper">
                            <?php foreach($subcats_classificados = get_field('categoria_subcategoria') as $subcat) : ?>
                              <?php $terms = get_term_by('id', $subcat, 'tipo'); ?>
                              <li><?php echo $terms->slug; ?></li>
                            <?php endforeach; ?>
                          </ul>
                        </div>
                      </article>
                  <?php endwhile; 
                echo '</div>',
              '</div>',
          '</div>';
    }

    wp_reset_postdata();

  ?>
<!-- carousel-classificados relacionados end -->