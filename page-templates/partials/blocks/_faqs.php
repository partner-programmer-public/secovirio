<?php 
  $assuntos = get_field( 'assuntos' );
  if ($assuntos) {
    foreach ($assuntos as $assunto) {
      echo '<p class="text-primary" id="'.sanitize_title($assunto['titulo']).'"><strong>'.$assunto['titulo'].'</strong></p><br>';
      $faqs = $assunto['perguntas_e_respostas'];

      if ($faqs) {
        for ($i=0; $i < count($faqs); $i++) {  
          $faq = $faqs[$i]; $count = $i + 1;
          echo '<p><strong>'.$count . '. ' . $faq['pergunta'].'</strong></p>';
          echo '<p>' . $faq['resposta'].'</p><br>';
        }
      }

    }
  }
?>