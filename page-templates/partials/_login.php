<?php $failed = isset($_GET['login']) == 'failed' ? 1 : 0;  ?>

<?php 
	// global $wp_query;
	// echo '<pre>'. print_r($wp_query, 1) . '</pre>';
	$placeholderUsuario = get_query_var( 'pagename') == 'cadastro-fornecedor' ? 'CNPJ' : 'CPF';
	// wp_login_form(['redirect' => 'teste']);
 ?>
<article <?php post_class(); ?> >
	<header>
		<?php
		  if ( function_exists('yoast_breadcrumb') ) {
		  	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
		  }
		?>
		<h1 class="page-title">
			<?php 
				$title = get_the_title();
				// Se o usuário estiver logado, vira página de atualizar
				if (is_user_logged_in()) { $title = 'Atualizar Cadastro'; }
				echo $title;				
			?>				
		</h1>
	</header>

	<section class="conteudo">
		<div class="">
			<?php the_field( 'texto_login' ); ?>
			<br><br>
		</div>
		<?php 
		if (isset($_GET['login'])) {			
			echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">';
				echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
				if ($_GET['login'] == 'empty') {
					echo '<p class="mb-0">Por favor, preencha todos os campos.</p>';
				} else {
					echo '<p class="mb-0">Dados não encontrados.<br /> Por favor, confira os dados digitados.</p>';
				}				
			echo '</div>';
			unset($_GET['login']);			
		}?>
		<form class="form-inline <?php echo $failed ? 'failed' : ''; ?>" action="<?php echo home_url(); ?>/wp-login.php" method="post">
				
				<label class="sr-only" for="log"><?php echo $placeholderUsuario; ?></label>
			  <input type="text" class="form-control mb-2 mr-sm-3 mb-sm-0 mask-<?php echo sanitize_title( $placeholderUsuario );?>" id="log" name="log" placeholder="<?php echo $placeholderUsuario; ?>">
				
				<?php if ($placeholderUsuario == 'CPF'): ?>
				
				<label class="sr-only" for="dt-nascimento">Data de Nascimento</label>
				<input type="text" class="form-control mb-2 mr-sm-3 mb-sm-0 mask-date" name="pwd" id="dt-nascimento" placeholder="Data de Nascimento:">

				<?php else: ?>
				
				<label class="sr-only" for="password">Senha</label>
				<input type="password" class="form-control mb-2 mr-sm-3 mb-sm-0" name="pwd" id="password" placeholder="Senha">

				<?php endif ?>
			  <input type="hidden" name="page-name" value="<?php echo get_query_var( 'pagename'); ?>">
			  <input type="hidden" name="redirect_to" value="<?php echo (isset($_GET['redirect_to']) && $_GET['redirect_to'] ? $_GET['redirect_to'] : get_permalink()); ?>">

			  <button type="submit" class="btn btn-rosaclaro mb-2 mb-sm-0">ENTRAR</button>
		</form>

	</section>
</article>