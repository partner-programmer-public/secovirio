<?php if (have_posts()) { ?>
		<div class="cpt-categories-wrapper">
			<div class="container-fluid">
				<?php echo '<h2 class="title">'.single_term_title( '', false ).' em todo o Rio de Janeiro</h2>'; ?>
				<div class="row">
					<?php 
						while ( have_posts() ) : the_post();
							print_anuncio_slide_block('col-12 col-md-3 mb-5', true);
						endwhile; 
					?>
				</div>
				<?php 
					if (function_exists('wp_pagenavi')) { 
						echo '<div class="clearfix"></div>';
						wp_pagenavi();
					}; 
				?>
			</div>
		</div>
	<?php }

	wp_reset_postdata();

?>