<?php 
	global $wp_query; 
	$obj = get_post_type_object($wp_query->get('post_type'));
?>
<header>
	<?php
	  if ( function_exists('yoast_breadcrumb') ) {
	  	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
	  }
	?>
	<h1 class="page-title color-seg h1">	
		<?php 
			if (is_tax()) {				
				echo single_term_title();
			} elseif (is_archive()) {				
				echo post_type_archive_title();
			} else {
				echo get_the_title();
			}
		 ?>					
	</h1>	
</header>			

<?php 				
		
		if (have_posts()): while (have_posts()) : the_post();
			get_partial('_loop-legislacao');
		endwhile; 
			if (function_exists('wp_pagenavi')) { 
				echo '<div class="clearfix"></div>';
				wp_pagenavi();
			};				
		endif; 
	
	wp_reset_query(); 

?>
