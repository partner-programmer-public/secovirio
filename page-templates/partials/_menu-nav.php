<?php 
  $mobileLogo = get_field('mobile_logotipo', get_option( 'page_on_front' ));
?>
  <nav id="navmenu" class="navbar navbar-expand-lg navbar-light animation-close-toggle">
    <a class="navbar-brand d-xl-none mr-0" href="<?php echo get_home_url(); ?>">     
      <?php if ($mobileLogo): ?>
         <img src="<?php echo $mobileLogo; ?>" class="d-inline-block align-top" alt="Secovi Rio - Sindicato da Habitação">
       <?php endif ?>       
       <span class="sr-only"><?php echo get_bloginfo( 'name' ) ?></span>
    </a>
    <i class="fa fa-search search-toggler js-toggle-search"></i>   
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span></span>
      <span></span>
      <span></span>
    </button>      
      <?php 
        wp_nav_menu( array(         
          'theme_location'    => 'global',
          'depth'             => 2,
          'container'         => 'div',
          'container_class'   => 'collapse navbar-collapse',
          'container_id'      => 'navbarNav',
          'menu_class'        => 'navbar-nav flex-sm-row justify-content-sm-around justify-content-xl-between',
          'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
          'walker'            => new WP_Bootstrap_Navwalker()
        )); 
      ?>     
  </nav><!-- /.navbar-collapse -->    