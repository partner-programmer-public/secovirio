<article <?php post_class(); ?> >
	<header>
		<?php
		  if ( function_exists('yoast_breadcrumb') ) {
		  	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
		  }
		?>
		<h1 class="page-title"><?php the_title(); ?></h1>
	</header>
	
	<div class="conteudo">
		<?php 
			the_content(); 						
			
			// Se for FAQ's
			if (is_page_template('page-templates/page-faqs.php')) {			
				get_block('_faqs');
			} elseif (is_page_template('page-templates/page-downloads-com-filtro.php')) {
				get_partial('_acf-download-w-filter-and-search');
			} elseif (is_page('banco-de-curriculos')) {
				if (current_user_can('manage_options') || is_membro()) {
					echo 	'<br><a href="'.get_permalink().'/pesquisar" class="btn btn-secondary">',
									'PESQUISAR EM CURRICULOS CADASTRADOS',
								'</a>';
				}
			} elseif (is_page('fornecedores')) {
				get_partial('_loop-fornecedores');
			} else {
				global $post;
				$children = get_pages( array( 'child_of' => $post->ID ) );
				if (
						!$post->post_content // Se não tiver conteudo
						&& $children // Se exifir filhos
				) {
					 echo do_shortcode('[paginas-filhas]');
				}	 
			}
			
			// Válido para página com listagem de itens
			if (get_field( 'items' )) {
				get_block('_lista-items');
			}
		?>
	</div>
	
	<footer>
		<?php get_block('_comments-disqus'); ?>
	</footer>

</article>