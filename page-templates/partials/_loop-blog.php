<article <?php post_class('box-post'); ?>>
	<?php if (has_post_thumbnail() && !is_single()): ?>
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="thumb" <?php echo thumbnail_bg() ?>> </a>
	<?php endif; ?>
	<div class="content <?php if (!has_post_thumbnail()) { echo 'large'; } ?>">
		<div class="box-align">			
			<a href="<?php the_permalink(); ?>" title="Saiba mais sobre: <?php the_title(); ?>">
				<h1><?php the_title(); ?></h1>
			</a>
			<ul class="infos">
				<li>Por Secovi Rio<?php // echo get_the_author_meta('display_name'); ?> - <time><?php echo get_the_date(); ?></time></li>
			</ul>
			<?php if (!is_single()): ?>
				<a href="<?php the_permalink(); ?>" title="Saiba mais sobre: <?php the_title(); ?>" class="hidden-sm hidden-xs p1">
					<?php echo get_the_excerpt(); ?>
					<br><br>
				</a>				
			<?php endif ?>			
		</div>
	</div>
</article>