<?php $current_user = wp_get_current_user(); ?>
<article <?php post_class(); ?> >
	<header>
		<?php
		  if ( function_exists('yoast_breadcrumb') ) {
		  	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
		  }
		?>
		<h1 class="page-title">
			<?php 
				$title = get_the_title();
				// Se o usuário estiver logado, vira página de atualizar
				if (is_user_logged_in() && !current_user_can('administrator')) {
					$title = 'Atualizar Cadastro';
				}
				echo $title;
			?>				
		</h1>
	</header>
	
	<div class="conteudo">
		<?php 
			the_content(); 			 
			global $post;
			$args = [
				'post_id' => 'new_user',
				'form_attributes' => [
					'class' => 'acf-form form-cadastro'
				],
				'submit_value' => 'CADASTRAR',
				'uploader' => 'basic',
				'html_submit_button'	=> 
					'<input type="submit" class="btn btn-rosaclaro w-100" value="%s" />',					
				'html_after_fields' =>
					'<input type="hidden" name="user_type" value="'.$post->post_name.'">',
				'html_updated_message'  => '<div class="alert alert-success alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'. (is_user_logged_in() ? get_field( 'retorno_atualizacao' ) : get_field( 'retorno_novo' ) ) .'</div>'
			];
			// FORNECEDOR
			if ($post->post_name == 'cadastro-fornecedor') {
				$args['fields'] = [
					'field_596cf588ea2f5', // EMPRESA
					'field_596cf5a5ea2f6', // CNPJ
					'field_59db12d76f2ed', // CATEGORIA
					'field_59db13b46f2f3', // E-MAIL
					'field_596cf676ea2fe', // SENHA
					'field_59db136c6f2f2', // SITE
					'field_596cf62dea2fa', // TELEFONE
					'field_596cf61dea2f9', // ENDERECO
					'field_59db13306f2ef', // COMPLEMENTO
					'field_59db131b6f2ee', // NUMERO
					'field_59db133c6f2f0', // BAIRRO
					'field_59db134d6f2f1', // CIDADE
					'field_596cf68eea2ff', // LOGOTIPO DA EMPRESA
					'field_596cf5d5ea2f8', // DESCRICAO
				]; 				
			} elseif ( $post->post_name == 'cadastrar-curriculo') {
				$args['fields'] = [
					'field_59db41b8418fd', // NOME COMPLETO
					'field_59db41b841c8c', // EMAIL
					'field_59db41b841cf5', // TELEFONE
					'field_59db4217b2757', // NASCIMENTO
					'field_59db4ba77ba9e', // CPF
					'field_59db41b841bb9', // CIDADE
					'field_59db41eeb2756', // ESTADO
					'field_59db41b8419d3', // AREA
					'field_59db4ab87ba9c', // FUNCAO
					'field_59db4bc27ba9f', // PRETENSAO SALARIAL
					'field_59db4a6a7ba9a', // ESCOLARIDADE					
					'field_59db4b487ba9d', // EXPERIENCIA PROFISSIONAL
					'field_59db41b841e2e', // OBJETIVOS
					'field_59db41b841dc8', // ANEXO
				]; 
			}

			// Se o usuário estiver logado, vira página de atualizar
			if (is_user_logged_in() && !current_user_can('administrator') ) {
				$args['post_id'] = 'user_' . $current_user->ID;
			}

			// echo '<pre>'. print_r($post, 1) . '</pre>';
			
			acf_enqueue_uploader();
			acf_form($args);
		?>
	</div>
	
</article>