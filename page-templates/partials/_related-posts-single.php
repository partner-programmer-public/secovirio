<?php 	
	global $post;
	$terms = get_the_category( $post->ID);
	$termsArray = array();
	for ($i=0; $i < count($terms); $i++) { 
			$termsArray[] = $terms[$i]->term_id;
	}
	$terms = implode(',', $termsArray);
	// echo '<pre>'. print_r($terms, 1) . '</pre>';
	
	$categoria_relacionada = $terms;
	$related = pp_get_related_posts_by_category( $categoria_relacionada, 2 );

	if ( $related->have_posts() ):
		?>
		<div class="related-posts mt-4">
			<h2 class="mb-4 h4">Veja também:</h2>
			<ul class="list-inline row">
				<?php while ( $related->have_posts() ): $related->the_post(); ?>
					<li class="col-md-6">
						<a href="<?php the_permalink(); ?>" title="<?php the_title() ?>">
							<?php 
								if (has_post_thumbnail()):
									the_post_thumbnail( 'thumb-related', array('class' => 'img-fluid') );
								endif;
							?>								
							<h3 class="h6 text-bodytext mt-3"><?php the_title(); ?></h3>
							<?php // the_excerpt(); ?>
						</a>
					</li>
				<?php endwhile; ?>
			</ul>
			<!-- <div class="text-right box-btn">
				<a href="<?php echo home_url(); ?>/blog" class="btn btn-primary">
					Ver mais no blog > 
				</a>
			</div> -->
		</div>
		<?php
	endif;
	wp_reset_postdata();


	// echo '<pre>'. print_r($post, 1) . '</pre>';	
?>