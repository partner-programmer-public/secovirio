<?php 
	$fornecedor = get_user_by( 'slug', get_query_var( 'author_name' ) );
	$current_user = wp_get_current_user();
	$avatarID = get_user_meta($fornecedor->ID, 'logo_user', 1);
	$imgSrc = wp_get_attachment_image_src( $avatarID, 'thumbnail');
?>
<article <?php post_class(''); ?>>
	<header>	
		<div class="row">
			<div class="col-12">
				<p id="breadcrumbs">
					<span xmlns:v="http://rdf.data-vocabulary.org/#">
						<span typeof="v:Breadcrumb">
							<a href="<?php echo home_url(); ?>" rel="v:url" property="v:title">Início</a>
							<i class="fa fa-angle-right"></i>
							<span rel="v:child" typeof="v:Breadcrumb">
								<a href="<?php echo get_permalink(78); ?>" rel="v:url" property="v:title">Fornecedores</a>
								<i class="fa fa-angle-right"></i>
								<span class="breadcrumb_last"><?php echo get_user_meta($fornecedor->ID, 'empresa_user', 1 ); ?></span>
							</span>
						</span>
					</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php if ($avatarID): ?>
					<div class="box-avatar">
						<img src="<?php echo $imgSrc[0]; ?>" class="mx-auto d-block" alt="Logotipo Empresa">
					</div>
				<?php endif ?>	
			</div>
			<div class="col-md-9">
				<h1 class="page-title color-seg h4">	
					<?php echo get_user_meta($fornecedor->ID, 'empresa_user', 1 ); ?>					
				</h1>	
				<p class="text-primary">
					<?php 
							if (get_user_meta($fornecedor->ID, 'categoria_user', 1)) {									
								echo get_user_meta($fornecedor->ID, 'categoria_user', 1) . '<br>';
							}							
					 ?>
				</p>
			</div>
		</div>		
	</header>
	
	<div class="content">
		<div class="row">
			<div class="col-md-3">
				
			</div>
			<div class="col-md-9">
				<?php 
					$fields = [
						'Endereço:' => 'endereco_user',
						'Número:' => 'numero_user',
						'Complemento:' => 'complemento_user',
						'Bairro:' => 'bairro_user',
						'Cidade:' => 'cidade_user',
						'Site:' => 'site_user',
						'E-mail:' => 'e-mail_user',
						'Telefone:' => 'telefone_user',
						'CNPJ:' => 'cnpj_user',
						'Descrição:' => 'descricao_user'
					];

					foreach ($fields as $label => $key) {
						if (get_user_meta($fornecedor->ID, $key, 1)) {
							echo 	'<p>',
											'<strong>'.$label.'</strong><br>',
											get_user_meta($fornecedor->ID, $key, 1),
										'</p>';
						}
					}					
					

					$recomendacoes = new WP_Query( [
							'post_type' => 'recomendacao', 'posts_per_page' => -1,
							'meta_query' => [
								['key' => 'fornecedor_id', 'value' => $fornecedor->ID ]
							]
					] ); 

					echo 	'<h2 id="recomendacoes" class="text-primary section-title">',
									'Recomendações ('.$recomendacoes->post_count.')</h2>';

					if ($recomendacoes->have_posts()) {
						echo '<div class="list-recomendacoes">';
						
						while ( $recomendacoes->have_posts() ) : $recomendacoes->the_post(); 
							global $post;
							echo 	'<div class="recomendacao" id="recomendacao-'.$post->ID.'">',
											'<p class="title">',
													'<strong>'.get_user_meta($post->post_author, 'empresa_user', 1).'</strong>',
											'</p>';
										the_content();
							echo 	'</div>';
						endwhile; 

						echo '</div>';
					}			
					wp_reset_postdata();

				 ?>
				
				

				<?php 
					if (
							is_user_logged_in() && 
							get_user_meta($post->post_author, 'user_type', 1) != 'cadastro-fornecedor'
						) :
						echo '<p class="alert alert-warning small">Apenas fornecedores podem deixar recomendações.</p>';
					
					elseif (is_user_logged_in() && ($current_user->ID != $fornecedor->ID)): 
						
						// Se for enviado algum parâmetro, cria uma nova recomandação
						if (isset($_POST['post_content'])) {
							
							$postarr = [
								'post_title' => 
									wp_strip_all_tags(
										get_user_meta($current_user->ID, 'empresa_user',1) . ' sobre ' . 
										get_user_meta($fornecedor->ID, 'empresa_user',1)
									),
								'post_content' => wp_strip_all_tags($_POST['post_content']),
								'post_status' => 'publish',
								'post_type' => 'recomendacao',
								'meta_input' => [
									'fornecedor_id' => $fornecedor->ID
								]
							];
							$post_id = wp_insert_post( $postarr);
							// echo '<pre>'. print_r($post_id, 1) . '</pre>';							
							echo '<div class="alert alert-success alert-dismissible fade show mt-4" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><small><strong>Sucesso!</strong> Sua recomendação foi inserida com sucesso.</small></div>';
							unset($_POST['post_content']);
						}
					?>
				 	<form action="" method="post" class="mt-4" accept-charset="utf-8">
				 		<div class="form-group">
				 			<textarea name="post_content" placeholder="Deixe sua recomendação sobre esse fornecedor" rows="5" class="form-control"></textarea>
				 		</div>
				 		<input type="submit" name="submit" value="ENVIAR" class="btn btn-rosaclaro w-100">

				 	</form>
				<?php elseif($current_user->ID == $fornecedor->ID) : ?>
					<p class="alert alert-warning small">Você não pode deixar uma recomandação para sua empresa.</p>
				<?php else: ?>
					<h6>Faça login para deixar sua recomendação.</h6>
				<?php endif ?>
			</div>
		</div>
	</div>
</article>
