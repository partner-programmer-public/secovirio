<article <?php post_class(); ?>>	
	<?php 
		echo  '<h1>'.get_the_title().'</h1>';
					the_content();

					$arquivo = get_field( 'arquivo' );
													
					if ($arquivo) {
						echo 	'<a target="_blank" download class="btn btn-rosaclaro" title="Veja mais sobre" href="'.$arquivo.'">',
										'BAIXAR',
									'</a>';
					}		
	?>
</article>