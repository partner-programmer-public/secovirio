<?php 
/*
* Template name: Contato
*/
get_header();
?>
	<main id="main-content" class="main" role="main">	
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<article <?php post_class('page-contato'); ?> >
				<div class="container">					
					<div class="row">
						<div class="col-md-6 col-lg-4" id="bg-full">
							<header class="pb-2">
								<h1 class="page-title color-seg h1 text-center"> 
									<?php the_title(); ?>
								</h1>
							</header><!-- /header -->
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</article>
		<?php endwhile; // end of the loop. ?>

		<?php if (get_field( 'contatos' )): ?>
			<div class="bg-secondary" id="area-contatos">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col col-lg-9">
							<?php if (get_field( 'titulo_secao' )): ?>
								<h2 class="text-center text-white h1">
									<?php the_field( 'titulo_secao' ); ?>							
								</h2>
								<div class="row">
									<?php
										$contatos = get_field( 'contatos' );
											array_map(function($contato){
												echo 	'<div class="col col-md-6 col-lg-4">',
																'<p class="text-white">',
																	'<strong>'.$contato['titulo'].'</strong><br>',
																	$contato['conteudo'],
																'</p>',
															'</div>';
											}, $contatos);
										// echo '<pre>'. print_r($contatos, 1) . '</pre>';								
									?>
								</div>
							<?php endif ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif ?>

	</main> <!-- #content -->

	<?php if (has_post_thumbnail()): ?>
		<style type="text/css" media="screen">
			#bg-full:after {
				background-image: url('<?php echo get_the_post_thumbnail_url() ?>');
			}
		</style>
	<?php endif ?>

<?php get_footer(); ?>