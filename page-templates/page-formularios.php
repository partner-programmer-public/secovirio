<?php 
/*
* Template name: Formulários
*/

	// Evita Cross-site scripting nos fields
	function my_kses_post( $value ) {
		if( is_array($value) ) { return array_map('my_kses_post', $value); }
		return wp_kses_post( $value );
	}
	add_filter('acf/update_value', 'my_kses_post', 10, 1);

	acf_form_head();	
	get_template_part('index');
?>