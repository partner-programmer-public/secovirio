<?php 
	/*
	* Template name: Classificados
	*/
?>
<?php get_header(); ?>
<?php get_block('_slideshow-classificados'); ?>
<main id="main-content" class="main classificados" role="main">
	<?php 
		get_block('_classificados-menu');
		print_carousel_anuncios_em_destaque('produtos');
		get_block('_classificados-banner');
		print_carousel_anuncios_em_destaque('servicos');
		print_carousel_anuncios_em_destaque('oportunidades');
	?>
</main>
<?php get_footer(); ?>