<?php
/**
 * Template Name: Home
 *
 */

	get_header();
	get_block('_slideshow');
?>	
	<main id="content" role="main">
		<?php get_block('_carousel-last-posts'); ?>

		
		<?php 
			$today = date('Ymd');
			$cpts = new WP_Query( [
				'post_type' => 'agenda', 'posts_per_page' => 5, 'orderby'	=> 'meta_value_num',
		'order'		=> 'ASC',
		'meta_query' => array(								
		    array(
	        'key'		=> 'data',
	        'compare'	=> '>=',
	        'value'		=> $today,
		    )),
			]); 
			// echo '<pre>'. print_r($cpts, 1) . '</pre>';
			

			if ($cpts->have_posts()) {
				echo  '<div id="section-cursos-wrapper" '.get_acf_thumbnail_bg('imagem_de_fundo').'>',
								'<div class="container">',
									'<h2>Cursos</h2>',
									'<div class="row">',
										'<div class="col-md-6 col-xl-5">';
											while ( $cpts->have_posts() ) : $cpts->the_post();
												 $classificacao = wp_get_post_terms(
												 	get_the_id(), 'classificacao', array("fields" => "names"));					 
												 // get raw date
												 $data = get_field('data', false, false);
												 // make date object
												 $date = new DateTime($data);
												 echo 	'<a href="'.get_permalink().'" class="d-flex" title="Saiba mais">',
												 						'<time datetime="'.$data.'">',
												 							'<span>'.date_i18n("d", $date->getTimestamp()).' </span>',
												 							date_i18n("M", $date->getTimestamp()),
												 						'</time>',
												 						'<span>',
												 							'<p>',
												 								get_the_title(),
												 							'</p>',
												 							'<span class="text-rosaclaro">',
												 								(is_array($classificacao) ? implode(',', $classificacao) : ''),
												 							'</span>',
												 						'</span>',
												 				'</a>';
												endwhile; 		        				
												wp_reset_postdata();
						// if ($cpts->max_num_pages > 1) {
							echo 	'<div class="d-flex justify-content-end">',
												'<a href="'.get_post_type_archive_link('agenda').'" class="btn btn-azulsecovi btn-icon btn-icon-right" title="Veja mais cursos">',
												'Veja mais cursos',
												'<i class="fa fa-arrow-right bg-primary"></i>',											
											'</a>',
										'</div>';
						// }
				echo  '</div>',
							'</div>',
							'</div>',
							'</div>';
			}
			
		?>


		  
			<center> <p style="font-size:1rem; text-color:#6d767c;  font-weight: bold; padding-top: 30px">REVISTA SECOVIRIO</p> </center>
			<?php

			$items = get_field( 'categorias' );
			// $current = isset($_GET['categoria']) ? $_GET['categoria'] : 0 ;		
			$listItems = array_reduce(array_column($items, 'itens'), 'array_merge', []);
			
			// $categorias = new WP_Query( [
			// 	'post_type' => 'post', 'post__in' => $categorias,
			// 	'orderby' => 'post__in'
			// ] );

			?> 
				    
  			<?php if( have_rows('categorias') ): ?>
		  
		    	 <div id="carousel-secoes-wrapper">
		      	<div class="container">
		        	<div class="slick slick-secoes">
		            	<?/*php while ( have_rows('categorias',$homeID) ) : the_row();  */?>
		            		<?php foreach ($listItems as $item) {  

								$href = $item['tipo'] == 'url_externa' ? $item['url'] : $item['arquivo'];
								$imgSrc = $item['capa']['sizes']['thumb-download'];
		            		
		            		?>
		             	  <div>
		                	<div class="wrapper">
		                  	<h2 style=" font-size: 1rem; color:#003470"> <?php echo($item['titulo']);?> </h2>
		                  	<img src="<?php echo($imgSrc); ?>" class="img-fluid mx-auto d-block">	                
		                       <br><a class="btn btn-secondary" href="<?php echo($href); ?>" target=" _blank"> LER A REVISTA  </a>
		                   
		               </div>
		              </div>
		              <?php 
		         		}; 
		         	  ?>

		          </div>
		          	<div class="d-flex justify-content-end">
						<a href="https://www.secovirio.com.br/noticias/publicacoes/?categoria=revistas" class="btn btn-azulsecovi btn-icon btn-icon-right" title="Veja mais" style="margin-bottom: 20px"> Ver outras edições <i class="fa fa-arrow-right bg-primary"></i></a>
					</div>
		        </div>
		      </div>
		    </div>
			<?php endif; ?>

		<!-- fim revistas !-->

				<div id="sobre-nos-wrapper" <?php acf_thumbnail_bg('imagem_de_fundo_sobrenos') ?>>
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-xl-5">
						<div class="wrapper bg-white">
							<h2 class="text-secondary"><?php the_field('titulo_sobre_nos') ?></h2>
							<p><?php the_field('conteudo_sobre_nos') ?></p>
							<?php 
							    $link = get_field('botao_sobre_nos');
							    if( $link ): 
							?>
							  <div class="d-flex justify-content-end">
							  	<a class="btn btn-rosaclaro btn-icon btn-icon-right" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
							  		<i class="fa fa-arrow-right bg-primary"></i>
							  		<?php echo $link['title']; ?>						  
							  	</a>
							  </div>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-6 col-xl-7 embed-responsive">
							<?php the_field('video_sobre_nos') ?>
					</div>
				</div>
			</div>
		</div>

		<?php get_block('_carousel-secoes'); ?>
	



	</main> <!-- #content -->
<?php get_footer(); ?>
