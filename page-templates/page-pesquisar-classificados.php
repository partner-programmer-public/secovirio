<?php 
/*
* Template name: Pesquisar Classificados
*/
?>

<?php get_header(); ?>
	<main id="main-content" class="main classificados categorias pesquisa" role="main">
		<?php get_block('_classificados-menu'); ?>
		<?php get_block('_classificados-search'); ?>

		<div class="results">
				<div class="container-fluid">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					}
				?>
					<?php echo '<h2 class="title"> 9999 anúncios encontrado em %termo_buscado%</h2>'; ?>
					<div class="row">
						<article class="col-12 col-md-3">
							<a class="mx-auto d-block" href="https://sercovirio.local/classificado/teste-13/" title="Hugo Rafael Souza de Lima">
								<h4>Hugo Rafael Souza de Lima</h4>
								<p class="string_limit medium">Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem esse dicta ex? Recusandae, doloribus ad! Deserunt quisquam aperiam velit voluptatum aliquid. Vel tenetur accusamus rem alias</p>
							</a>
							<div class="texts-wrapper">
								<p class="tel">(81) 995694358</p>
								<p class="email">hugor.souzalima@gmail.com</p>
								<p class="site">https://hugorlima.com.br</p>
								<ul class="tags-list-wrapper">
									<li>produtos</li>
								</ul>
							</div>
						</article>
						<article class="col-12 col-md-3">
							<a class="mx-auto d-block" href="https://sercovirio.local/classificado/teste-13/" title="Hugo Rafael Souza de Lima">
								<h4>Hugo Rafael Souza de Lima</h4>
								<p class="string_limit medium">Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem esse dicta ex? Recusandae, doloribus ad! Deserunt quisquam aperiam velit voluptatum aliquid. Vel tenetur accusamus rem alias</p>
							</a>
							<div class="texts-wrapper">
								<p class="tel">(81) 995694358</p>
								<p class="email">hugor.souzalima@gmail.com</p>
								<p class="site">https://hugorlima.com.br</p>
								<ul class="tags-list-wrapper">
									<li>produtos</li>
								</ul>
							</div>
						</article>
						<article class="col-12 col-md-3">
							<a class="mx-auto d-block" href="https://sercovirio.local/classificado/teste-13/" title="Hugo Rafael Souza de Lima">
								<h4>Hugo Rafael Souza de Lima</h4>
								<p class="string_limit medium">Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem esse dicta ex? Recusandae, doloribus ad! Deserunt quisquam aperiam velit voluptatum aliquid. Vel tenetur accusamus rem alias</p>
							</a>
							<div class="texts-wrapper">
								<p class="tel">(81) 995694358</p>
								<p class="email">hugor.souzalima@gmail.com</p>
								<p class="site">https://hugorlima.com.br</p>
								<ul class="tags-list-wrapper">
									<li>produtos</li>
								</ul>
							</div>
						</article>
						<article class="col-12 col-md-3">
							<a class="mx-auto d-block" href="https://sercovirio.local/classificado/teste-13/" title="Hugo Rafael Souza de Lima">
								<h4>Hugo Rafael Souza de Lima</h4>
								<p class="string_limit medium">Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem esse dicta ex? Recusandae, doloribus ad! Deserunt quisquam aperiam velit voluptatum aliquid. Vel tenetur accusamus rem alias</p>
							</a>
							<div class="texts-wrapper">
								<p class="tel">(81) 995694358</p>
								<p class="email">hugor.souzalima@gmail.com</p>
								<p class="site">https://hugorlima.com.br</p>
								<ul class="tags-list-wrapper">
									<li>produtos</li>
								</ul>
							</div>
						</article>
					</div>
					<?php /* if (function_exists('wp_pagenavi')) { 
						echo '<div class="clearfix"></div>';
						wp_pagenavi( array( 'query' => $query ));
					}; */ ?>
				</div>
			</div>


	</main>
<?php get_footer(); ?>