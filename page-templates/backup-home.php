<?php
/**
 * Template Name: Backup Home
 *
 */

	get_header();
	get_block('_slideshow');
?>	
	<main id="content" role="main">
		<?php get_block('_carousel-last-posts'); ?>

		
		<?php 
			$today = date('Ymd');
			$cpts = new WP_Query( [
				'post_type' => 'agenda', 'posts_per_page' => 5,
				'meta_query' => array(array('key'		=> 'data','compare'	=> '>=','value'		=> $today)),
			]); 
			// echo '<pre>'. print_r($cpts, 1) . '</pre>';
			

			if ($cpts->have_posts()) {
				echo  '<div id="section-cursos-wrapper" '.get_acf_thumbnail_bg('imagem_de_fundo').'>',
								'<div class="container">',
									'<h2>Cursos</h2>',
									'<div class="row">',
										'<div class="col-md-6 col-xl-5">';
											while ( $cpts->have_posts() ) : $cpts->the_post();
												 $classificacao = wp_get_post_terms(
												 	get_the_id(), 'classificacao', array("fields" => "names"));					 
												 // get raw date
												 $data = get_field('data', false, false);
												 // make date object
												 $date = new DateTime($data);
												 echo 	'<a href="'.get_permalink().'" class="d-flex" title="Saiba mais">',
												 						'<time datetime="'.$data.'">',
												 							'<span>'.date_i18n("d", $date->getTimestamp()).' </span>',
												 							date_i18n("M", $date->getTimestamp()),
												 						'</time>',
												 						'<span>',
												 							'<p>',
												 								get_the_title(),
												 							'</p>',
												 							'<span class="text-rosaclaro">',
												 								(is_array($classificacao) ? implode(',', $classificacao) : ''),
												 							'</span>',
												 						'</span>',
												 				'</a>';
												endwhile; 		        				
												wp_reset_postdata();
						// if ($cpts->max_num_pages > 1) {
							echo 	'<div class="d-flex justify-content-end">',
												'<a href="'.get_post_type_archive_link('agenda').'" class="btn btn-azulsecovi btn-icon btn-icon-right" title="Veja mais cursos">',
												'Veja mais cursos',
												'<i class="fa fa-arrow-right bg-primary"></i>',											
											'</a>',
										'</div>';
						// }
				echo  '</div>',
							'</div>',
							'</div>',
							'</div>';
			}
			
		?>


		<?php get_block('_carousel-secoes'); ?>
	

		<div id="sobre-nos-wrapper" <?php acf_thumbnail_bg('imagem_de_fundo_sobrenos') ?>>
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-xl-5">
						<div class="wrapper bg-white">
							<h2 class="text-secondary"><?php the_field('titulo_sobre_nos') ?></h2>
							<p><?php the_field('conteudo_sobre_nos') ?></p>
							<?php 
							    $link = get_field('botao_sobre_nos');
							    if( $link ): 
							?>
							  <div class="d-flex justify-content-end">
							  	<a class="btn btn-rosaclaro btn-icon btn-icon-right" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
							  		<i class="fa fa-arrow-right bg-primary"></i>
							  		<?php echo $link['title']; ?>						  
							  	</a>
							  </div>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-6 col-xl-7 embed-responsive">
							<?php the_field('video_sobre_nos') ?>
					</div>
				</div>
			</div>
		</div>

	</main> <!-- #content -->
<?php get_footer(); ?>
