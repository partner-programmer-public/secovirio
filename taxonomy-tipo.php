<?php get_header(); ?>
<?php get_block('_slideshow-classificados-cats'); ?>

<?php
	$term = get_queried_object();
	$slider = get_field('slideshow_classificados_categorias', $term->taxonomy . '_' . $term->term_id);
?>

	<main id="main-content" class="main classificados categorias<?php if( !$slider ){ echo ' no-slider'; } ?>" role="main">
		<?php 
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;			
			get_block('_classificados-menu');
			get_block('_classificados-search');

			if ($paged == 1) {
				print_carousel_anuncios_em_destaque();
				get_block('_classificados-banner');
				get_partial('loop-classificados');
			} else {				
				get_block('_classificados-banner');
				get_partial('loop-classificados');
			}
		?>
	</main>
<?php get_footer(); ?>