<?php

add_shortcode( 'button', 'button_shortcode' );
 
function button_shortcode( $atts ) {
   extract( shortcode_atts(
     array(
       'titulo' => '',
       'url' => '',
       'target' => ''
     ),
     $atts
   ));
   return '<a class="btn btn-rosaclaro" target="'.$target.'" title="' . $titulo . '" href="' . $url . '">' . $titulo . '</a>';
}


shortcode_ui_register_for_shortcode(
    'button', 
        array(
        // 'post_type'     => array( 'post', 'page' ),
        'label' => 'Botão',
        'listItemImage' => 'dashicons-video-alt3', /* Optional. src or dashicons-$icon.  */
        /** Shortcode Attributes */
        'attrs'          => array(  
            array(       
              /** This label will appear in user interface */
              'label'        => 'Título',         
              /** This is the actual attr used in the code used for shortcode */
              'attr'         => 'titulo',         
              /** Define input type. Supported types are text, checkbox, textarea, radio, select, email, url, number, and date. */
              'type'         => 'text',         
              /** Add a helpful description for users
              'description'  => 'Please enter the button text',
              ), */                      
            ),
            array(
              'label'        => 'Qual url do botão?',
              'attr'         => 'url',
              'type'         => 'url',
              'class'        => 'regular-text'
            ),
            array(
              'label'        => 'Abrir em  uma nova janela?',
              'attr'         => 'target',
              'type'         => 'radio',
              'value'      => '_self',
              'options'      => array('_self' => 'Não', '_blank' => 'Sim')
            ),
        ),        
    )
);


/* Botão PDF */
/* ----------------------------------------- */

    add_shortcode( 'botao-pdf', 'botaopdf_shortcode' );
     
    function botaopdf_shortcode( $atts ) {
       extract( shortcode_atts(
         array(
           'titulo' => '',
           'url' => '',
           'target' => ''
         ),
         $atts
       ));

       ob_start();

       ?>

       <div class="d-flex shortcode-botaopdf">            
            <div>
                <img src="<?php images_url('icon-pdf.png') ?>" alt="Download PDF - <?php echo $titulo ?>">
            </div>
            <a class="align-self-center no-fancy" target="<?php echo $target; ?>" title="<?php echo $titulo ?>" href="<?php echo $url; ?>">
                <strong>Clique aqui</strong> <?php echo $titulo ?>
            </a>
       </div>

       <?php

       return ob_get_clean();

    }

    shortcode_ui_register_for_shortcode(
        'botao-pdf', 
        array(
            // 'post_type'     => array( 'post', 'page' ),
            'label' => 'Download PDF',
            'listItemImage' => '<img src="'.get_images_url('icon-pdf.png').'">', /* Optional. src or dashicons-$icon.  */
            /** Shortcode Attributes */
            'attrs'          => array(  
                array(       
                  /** This label will appear in user interface */
                  'label'        => 'Título',         
                  /** This is the actual attr used in the code used for shortcode */
                  'attr'         => 'titulo',         
                  /** Define input type. Supported types are text, checkbox, textarea, radio, select, email, url, number, and date. */
                  'type'         => 'text',         
                  /** Add a helpful description for users */
                  'description'  => 'Insira o texto a ser inserido após o "Clique aqui".'
                ),
                array(
                  'label'        => 'Qual url do arquivo em PDF?',
                  'attr'         => 'url',
                  'type'         => 'url',
                ),
                array(
                  'label'        => 'Abrir em  uma nova janela?',
                  'attr'         => 'target',
                  'type'         => 'radio',
                  'value'      => '_blank',
                  'options'      => array('_self' => 'Não', '_blank' => 'Sim')
                ),
            ),  // attrs       
        )
    );
/* ----------------------------------------- Botão PDF */        






/* Páginas Filhas */
/* ----------------------------------------- */

    add_shortcode( 'paginas-filhas', 'child_pages_shorcode' );
     
    function child_pages_shorcode( $atts ) {
       extract( shortcode_atts(
         array('tipo' => ''),
         $atts
       ));

       ob_start();
        
        global $post;
        // Defaults 
        $args = [ 
          'child_of' => $post->ID,
          'title_li' => ''
        ];         
        $ulClass = 'list-unstyled';

        if ($tipo == 'veja-tambem') {
          $args['depth'] = 1;
          $ulClass .= ' veja-tambem';
          echo '<p class="veja-tambem-title text-primary"><strong>Veja também:</strong></p>';
        }


        echo '<ul class="'.$ulClass.'">';
          wp_list_pages($args);
        echo '</ul>';
        
       return ob_get_clean();

    }

    shortcode_ui_register_for_shortcode(
        'paginas-filhas', 
        array(
            // 'post_type'     => array( 'page' ),
            'label' => 'Páginas Filhas',
            'listItemImage' => 'dashicons-list-view', /* Optional. src or dashicons-$icon.  */
            /** Shortcode Attributes */
            'attrs'          => array(  
                array(
                  'label'        => 'Selecione o layout de exibição',
                  'attr'         => 'tipo',
                  'type'         => 'radio',
                  'value'        => 'simples',
                  'options'      => array(
                      'simples' => 'Lista simples', 
                      'veja-tambem' => 'Veja também'
                    )
                ),
            ),  // attrs       
        )
    );
/* ----------------------------------------- Páginas Filhas */        
