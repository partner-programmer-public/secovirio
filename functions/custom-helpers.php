<?php 

function get_query_anuncios_em_destaque($tipo) {

  $args = array(
    'post_type' => 'classificado',
    'posts_per_page'  => '-1',    
    'meta_query' => array(
      array(
        'key'   => 'destaque',
        'value' => '1',
      )
    )
  );

  if ($tipo) {
    $args['tax_query'] = array(
      array(
        'taxonomy' => 'tipo',
        'field'    => 'slug',
        'terms' => $tipo,
      )
    );
  }

  return new WP_Query( $args );  
}

// function get_anuncios_by_post_ids($post_ids) {  
//   $args = array(
//     'post_type' => 'classificado',
//     'post__in'  => $post_ids,
//   );
//   return new WP_Query( $args );
// }

// function get_anuncios_based_on_current_tipo() {

//   $queried_term = get_queried_object();    
//   $args = array(
//     'post_type' => 'classificado',
//     'posts_per_page'  => 4,
//     'tax_query' => array(
//       array(
//         'taxonomy' => 'tipo',
//         'field'    => 'slug',
//         'terms' => $queried_term->slug,
//       ),
//     ),
//   );
//   return new WP_Query( $args );
// }

function print_anuncio_slide_block($class = '', $hasDiv = false) { 
  global $post;   
?>
  <article class="<?php echo $class; ?>">
    <?php if ($hasDiv) { echo '<div>'; } ?>    
    <a class="mx-auto d-block" href="<?php echo get_permalink(); ?>" title="<?php echo get_field('nome'); ?>">
      <?php 
        $imagem = get_field('imagem');
        if ($imagem) : ?>
        <div class="image-wrapper">
          <img src="<?php echo $imagem['url']; ?>" alt="<?php echo $imagem['alt']; ?>">
        </div>
        <h4><?php echo get_field('nome'); ?></h4>
        <p class="string_limit small">
          <?php echo mb_strimwidth(get_field('descricao', false, false), 0, 62, "..."); ?>
        </p>
        <?php else: ?>
        <h4><?php echo get_field('nome'); ?></h4>
        <p class="string_limit medium">
        <?php echo mb_strimwidth(get_field('descricao', false, false), 0, 187, "..."); ?>
        </p>
      <?php endif; ?>
    </a>
    <div class="texts-wrapper">
      
      <?php if (get_field('telefone_ddd')) : ?>
      <p title="(<?php echo get_field('telefone_ddd'); ?>) <?php echo get_field('telefone_num'); ?>" class="tel">(<?php echo get_field('telefone_ddd'); ?>) <?php echo get_field('telefone_num'); ?></p>
      <?php endif ?>

      <?php if (get_field('whatsapp_num')) : ?>
        <p title="(<?php echo get_field('whatsapp_ddd'); ?>) <?php echo get_field('whatsapp_num'); ?>" class="wpp">(<?php echo get_field('whatsapp_ddd'); ?>) <?php echo get_field('whatsapp_num'); ?></p>
      <?php endif; ?>

      <p title="<?php echo get_field('email'); ?>" class="email"><?php echo get_field('email'); ?></p>
      
      <?php if (get_field('site')) : ?>
        <a title="<?php echo get_field('site'); ?>" href="<?php echo get_field('site'); ?>" target="_blank" class="site"><?php echo get_field('site'); ?></a>
      <?php endif; ?>
      
      <?php print_anuncio_tags_list() ; ?>

    </div>
    <?php if ($hasDiv) { echo '</div>'; } ?>
  </article>
<?php
}

function print_anuncio_tags_list() {
  global $post;   
  $subcats_classificados = get_field('categoria_subcategoria'); ?>
  <?php if ($subcats_classificados): ?>      
    <ul class="tags-list-wrapper">
      <?php foreach($subcats_classificados as $subcat) : ?>
        <?php $terms = get_term_by('id', $subcat, 'tipo'); ?>
        <li><?php echo $terms->slug; ?></li>
      <?php endforeach; ?>
    </ul>
  <?php endif;
}

function print_carousel_anuncios_em_destaque($tipo_slug = false) {
    
    $term = $tipo_slug ? get_term_by('slug', $tipo_slug, 'tipo' ) : get_queried_object();
    // echo '<pre>'.print_r($term,1). '</pre>';
    // die();
    if (!$term) return false;

    $query = get_query_anuncios_em_destaque($term->slug);
    if ($query->have_posts()) {

      echo  '<div class="carousel-classificados-wrapper">',
              '<h2 class="title">'.$term->name.' em destaque</h2>',
              '<div class="container-fluid">';

                echo '<div class="slick slick-classificados">';
                  while ( $query->have_posts() ) : $query->the_post();
                    print_anuncio_slide_block();
                  endwhile; 
                echo '</div>',
              '</div>';
              if ($tipo_slug) :
                echo
                  '<div class="btn-more-wrapper">',
                    '<a href="'.get_term_link($term, $term->taxonomy).'">Ver todos os '.$term->name.'</a>',
                  '</div>';
              endif;
      echo
          '</div>';
    }

    wp_reset_postdata();

}

function print_carousel_anuncios_relacionados($tipo_slug = false) {
    
    $term = $tipo_slug ? get_term_by('slug', $tipo_slug, 'tipo' ) : get_queried_object();
    // echo '<pre>'.print_r($term,1). '</pre>';
    // die();
    if (!$term) return false;

    $query = get_query_anuncios_em_destaque($term->slug);
    if ($query->have_posts()) {

      echo  '<div class="carousel-classificados-wrapper">',
              '<h2 class="title">Anúncios relacionados</h2>',
              '<div class="container-fluid">';

                echo '<div class="slick slick-classificados">';
                  while ( $query->have_posts() ) : $query->the_post();
                    print_anuncio_slide_block();
                  endwhile; 
                echo '</div>',
              '</div>',
          '</div>';
    }

    wp_reset_postdata();

}
?>