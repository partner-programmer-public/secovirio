<?php


/* Valida CNPJ no ACF */
/* ----------------------------------------- */
  add_filter('acf/validate_value/name=cnpj_user', 'pp_acf_valid_cnpj', 10, 4);

  function pp_acf_valid_cnpj( $valid, $value, $field, $input ){
    
    // bail early if value is already invalid
    if( !$valid ) { return $valid; }
    
    // Verificamos se o campo é inválido usando nossa função para validar CNPJ
    if (!valida_cnpj($value)) { $valid = "CNPJ Inválido!"; }   

    // Se for um novo usuário, valida se já existe um usuário com esse CNPJ
    if ($_POST['_acf_post_id'] == 'new_user') {
      if (username_exists(sanitize_title($value))) {
        $valid = 'Empresa já cadastrada com esse CNPJ';
      }
    }
    
    // retur
    return $valid;    
    
  }

/* ----------------------------------------- Valida CNPJ no ACF */  


/* Validações criação de usuário */
/* ----------------------------------------- */
  add_filter('acf/validate_value/name=e-mail_user', 'pp_acf_valid_user_email', 10, 4);

  function pp_acf_valid_user_email( $valid, $value, $field, $input ){
    
    // bail early if value is already invalid
    if( !$valid ) { return $valid; }
        
    // Se for um novo usuário, valida se já existe um usuário com esse CNPJ
    if ($_POST['_acf_post_id'] == 'new_user') {
      if (email_exists($value)) {
        $valid = 'Já existe um usuário registrado com esse e-mail.';
      }
    }    
    
    return $valid;    
    
  }


  add_filter('acf/validate_value/name=cpf_user', 'pp_acf_valid_user_cpf', 10, 4);

  function pp_acf_valid_user_cpf( $valid, $value, $field, $input ){
    
    // bail early if value is already invalid
    if( !$valid ) { return $valid; }
    
    // Verificamos se o campo é inválido usando nossa função para validar CPF
    if (!valida_cpf($value)) {
      $valid = "CPF Inválido!";
    }

    // Se for um novo usuário, valida se já existe um usuário com esse CPF
    if ($_POST['_acf_post_id'] == 'new_user') {
      if (username_exists(sanitize_title($value))) {
        $valid = 'Já existe um curriculo com esse CPF. Use a opção de atualizar.';
      }
    }  
    
    return $valid;    
    
  }

/* ----------------------------------------- Validações criação de usuário */    
    

/* Cria User pelo ACF-FORM */
/* ----------------------------------------- */
  function register_accueilli($post_id) {

    // Check if this is to be a new user
    if( $post_id != 'new_user' ) {
      return $post_id;
    }    

    // echo '<pre>'. print_r($_POST, 1) . '</pre>';
    // die();
    
    $f      = $_POST['acf'];
    $user_type = $_POST['user_type'];

    if ($user_type == 'cadastro-fornecedor') {
      $email  = $f['field_59db13b46f2f3']; // EMAIL
      $cnpj = $f['field_596cf5a5ea2f6']; // CNPJ
      $pseudo = sanitize_title($cnpj); // CNPJ
      $pass   = $f['field_596cf676ea2fe']; // PASSWORD  
      $first_name = ucwords(strtolower($f['field_596cf588ea2f5'])); // NOME EMPRESA
    } elseif ($user_type == 'cadastrar-curriculo') {
      $email  = $f['field_59db41b841c8c']; // EMAIL
      $cpf = $f['field_59db4ba77ba9e']; // CPF
      $pseudo = sanitize_title($cpf); // CPF
      $pass   = $f['field_59db4217b2757']; // PASSWORD (DT Nascimento)
      $first_name = ucwords(strtolower($f['field_59db41b8418fd'])); // NOME COMPLETO
    }
    
    
    if ( !username_exists($pseudo) && !email_exists($email) ) {

      $user_id         = wp_create_user( $pseudo, $pass, $email );
      $user_id_role    = new WP_User($user_id);

        update_usermeta( $user_id, 'user_type', $user_type );
        $updateUser = wp_update_user( array(
          'ID'            => $user_id,
          'first_name'    => $first_name,
        ));        

        /* ------  ADD THE DO_ACTION SAVE POST ------ */
        do_action('acf/save_post', "user_".$user_id);

    }

  }

  add_filter('acf/pre_save_post' , 'register_accueilli' );

/* ----------------------------------------- Cria User pelo ACF-FORM */    



/* Color picker no admin do ACF */
/* ----------------------------------------- */
function register_color_picker() { ?>
  
  <script type="text/javascript">
    (function($) {
      acf.add_filter('color_picker_args', function( args, $field ){
      
      // add the hexadecimal codes here for the colors you want to appear as swatches
      args.palettes = ['#1B51B5', '#FE5722']
      
      // return colors
      return args;
      });
    })(jQuery);
  </script>
  
  <?php }
  
  add_action('acf/input/admin_footer', 'register_color_picker');

  /* ----------------------------------------- Color picker no admin do ACF */