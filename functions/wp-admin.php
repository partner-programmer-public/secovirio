<?php

// ADMIN MENU
// YOAST
// DASHBOARD
// WP-LOGIN

/* ADMIN MENU */
/* ----------------------------------------- */
  // Change menu with fontawesome icons  
  // add_action('admin_head', 'fontawesome_icon_dashboard');

  function fontawesome_icon_dashboard() {
     echo "<style type='text/css' media='screen'>
        #adminmenu #menu-posts-produto div.wp-menu-image:before { font-family:'FontAwesome' !important; content:'\\f0a4'; }  
     </style>";
  }
/* ----------------------------------------- ADMIN MENU */    


/* YOAST */
/* ----------------------------------------- */
  // Filter Yoast Meta Priority
  add_filter( 'wpseo_metabox_prio', function() { return 'low';});
/* ----------------------------------------- YOAST */    


/* DASHBOARD */
/* ----------------------------------------- */
  // Remove default dashboard widgets
  add_action('admin_menu', 'disable_default_dashboard_widgets');


  function disable_default_dashboard_widgets() {
    remove_meta_box('dashboard_browser_nag', 'dashboard', 'core');
    remove_meta_box('dashboard_right_now', 'dashboard', 'core');
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'core');
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');
    remove_meta_box('dashboard_plugins', 'dashboard', 'core');
    remove_meta_box('dashboard_quick_press', 'dashboard', 'core');
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');
    remove_meta_box('dashboard_primary', 'dashboard', 'core');
    remove_meta_box('dashboard_secondary', 'dashboard', 'core');
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');    
  }

/* ----------------------------------------- DASHBOARD */    

/* WP-LOGIN */      
/* ----------------------------------------- */
  // Call wp-login Styles
  add_action( 'login_head', 'wpmidia_custom_login' );
  
  // Change URL logotype
  add_filter('login_headerurl', 'wpmidia_custom_wp_login_url');

  // Change title logotype
  add_filter('login_headertitle', 'wpmidia_custom_wp_login_title');

  function wpmidia_custom_login() {
      echo '<link media="all" type="text/css" href="'.get_template_directory_uri().'/assets/css/login-style.css" rel="stylesheet">';

      $logotipoID = get_post_meta(get_option( 'page_on_front' ), 'logotipo', 1);
      if ($logotipoID) {
      ?>
        <style type="text/css" media="screen">
          body.login h1 a {
            background-image: url(<?php echo wp_get_attachment_url($logotipoID); ?>);
            background-size: contain;
            background-position: center center;
          }
        </style>   
      <?php      
      }    
  }

  function wpmidia_custom_wp_login_url() {
    return home_url();
  }

  function wpmidia_custom_wp_login_title() {
    return get_option('blogname');
  }
/* ----------------------------------------- WP-LOGIN */    



/* Adiciona o ID do usuário no body-class */
/* ----------------------------------------- */
function id_usuario_body_class( $classes ) {
  global $current_user;
    $classes .= ' user-' . $current_user->ID;
  return trim( $classes );
}
add_filter( 'admin_body_class', 'id_usuario_body_class' );




/* Editor TINYMCE */
/* ----------------------------------------- */
  add_filter('tiny_mce_before_init', 'tiny_mce_remove_unused_formats' ); 

  function tiny_mce_remove_unused_formats($init) {
    // Add block format elements you want to show in dropdown
    $init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6';
    return $init;
  }

  function my_mce4_options($init) {
    $default_colours = '"000000", "Black",
                          "993300", "Burnt orange",
                          "333300", "Dark olive",
                          "003300", "Dark green",
                          "003366", "Dark azure",
                          "000080", "Navy Blue",
                          "333399", "Indigo",
                          "333333", "Very dark gray",
                          "800000", "Maroon",
                          "FF6600", "Orange",
                          "808000", "Olive",
                          "008000", "Green",
                          "008080", "Teal",
                          "0000FF", "Blue",
                          "666699", "Grayish blue",
                          "808080", "Gray",
                          "FF0000", "Red",
                          "FF9900", "Amber",
                          "99CC00", "Yellow green",
                          "339966", "Sea green",
                          "33CCCC", "Turquoise",
                          "3366FF", "Royal blue",
                          "800080", "Purple",
                          "999999", "Medium gray",
                          "FF00FF", "Magenta",
                          "FFCC00", "Gold",
                          "FFFF00", "Yellow",
                          "00FF00", "Lime",
                          "00FFFF", "Aqua",
                          "00CCFF", "Sky blue",
                          "993366", "Red violet",
                          "FFFFFF", "White",
                          "FF99CC", "Pink",
                          "FFCC99", "Peach",
                          "FFFF99", "Light yellow",
                          "CCFFCC", "Pale green",
                          "CCFFFF", "Pale cyan",
                          "99CCFF", "Light sky blue",
                          "CC99FF", "Plum"';

      $custom_colours =  '"FFFFFF", "Branco",
                          "FFFFFF", "Branco",
                          "6d767c", "Cinza",
                          "8e693f", "Primeira",
                          "003470", "Segunda",
                          "dcad77", "Terceira",
                          "cca06e", "Quarta"';

      // build colour grid default+custom colors
      $init['textcolor_map'] = '['.$default_colours.','.$custom_colours.']';

      // enable 6th row for custom colours in grid
      $init['textcolor_rows'] = 6;

      return $init;
  }
  add_filter('tiny_mce_before_init', 'my_mce4_options');

  
/* ----------------------------------------- Editor TINYMCE */    






add_action('save_post', 'assign_parent_terms', 10, 2);

function assign_parent_terms($post_id, $post){

    if($post->post_type != 'legislacao')
        return $post_id;

    // get all assigned terms   
    $terms = wp_get_post_terms($post_id, 'cat_legislacao' );
    foreach($terms as $term){
        while($term->parent != 0 && !has_term( $term->parent, 'cat_legislacao', $post )){
            // move upward until we get to 0 level terms
            wp_set_post_terms($post_id, array($term->parent), 'cat_legislacao', true);
            $term = get_term($term->parent, 'cat_legislacao');
        }
    }
}




/* Custom Page Login */
/* ----------------------------------------- */  
    
  function login_failed() {    
    wp_redirect( strtok($_SERVER['HTTP_REFERER'], '?'). '?login=failed' );
    exit;
  }
  add_action( 'wp_login_failed', 'login_failed' );
   
  function verify_username_password( $user, $username, $password ) {    
    
    if( isset($_POST['page-name']) && ($username == "" || $password == "" )) {
      wp_redirect( strtok($_SERVER['HTTP_REFERER'], '?'). '?login=empty' );
      exit;
    }

    // Se for na página cadastrar-curriculo
    if (
      isset($_POST['page-name']) && 
      ($_POST['page-name'] == 'cadastrar-curriculo')
    ) {

      // Remove os caracteres indesejados do CPF
      $userLogin = str_replace('.', '-', $username);
      // Verifica se existe usuário com o CPF enviado
      $myUser = get_user_by('login', $userLogin);
      
      if ($myUser && get_user_meta( $myUser->ID, 'nascimento_user', 1) == $password) {
        wp_set_auth_cookie($myUser->ID);
        wp_redirect( $_SERVER['HTTP_REFERER']);
        exit;
      }

    } elseif (
      isset($_POST['page-name']) && 
      ($_POST['page-name'] == 'cadastro-fornecedor')
    ) {
      
      // Remove os caracteres indesejados do CPF
      $userLogin = str_replace('.', '-', $username);      
      $userLogin = str_replace('/', '', $userLogin);      
      // Verifica se existe usuário com o CNPJ enviado
      $myUser = get_user_by('login', $userLogin);
      if ($myUser) {

        if (wp_check_password( $password,  $myUser->data->user_pass, $myUser->ID )) {
          return $myUser;
        }
        
      }
      
      // if ($myUser && get_user_meta( $myUser->ID, 'nascimento_user', 1) == $password) {
      //   wp_set_auth_cookie($myUser->ID);
      //   wp_redirect( $_SERVER['HTTP_REFERER']);
      //   exit;
      // }


    }



    
  }
  add_filter( 'authenticate', 'verify_username_password', 1, 3);

/* ----------------------------------------- Custom Page Login */    


// add_action( 'init', 'blockusers_init' );
// function blockusers_init() {
//   if ( is_admin() && ! current_user_can( 'administrator' ) &&
//     ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
//       wp_redirect( home_url() );
//       exit;
//     }
// }
  
  
  add_filter('user_contactmethods', 'modify_contact_methods');
    function modify_contact_methods($profile_fields) {
        // Add new fields
    // $profile_fields['facebook'] = 'Facebook URL';
    // $profile_fields['twitter'] = 'Twitter Username';
    // $profile_fields['gplus'] = 'Google+ URL';
    // $profile_fields['youtube'] = 'YouTube Channel URL';
      // echo '<pre>'. print_r($profile_fields, 1) . '</pre>';
      // die();
      
    // Remove old fields
      unset($profile_fields['facebook']);
      unset($profile_fields['twitter']);
      unset($profile_fields['googleplus']);

      return $profile_fields;
    }


/* Register Dashboard Widget */
/* ----------------------------------------- */
  /**
   *  Adds hidden content to admin_footer, then shows with jQuery, and inserts after welcome panel
   *
   *  @author Ren Ventura <EngageWP.com>
   *  @see http://www.engagewp.com/how-to-create-full-width-dashboard-widget-wordpress
   */
  add_action( 'admin_footer', 'rv_custom_dashboard_widget' );
  function rv_custom_dashboard_widget() {
    // Bail if not viewing the main dashboard page
    if ( get_current_screen()->base !== 'dashboard' ) {
      return;
    }
    ?>

    <div id="custom-id" class="welcome-panel" style="display: none;">
      <div class="welcome-panel-content">
        <h2>Cadastros</h2>
        <br>
        <div class="cadastro-usuarios">
          <a target="_blank" href="<?php echo get_permalink(857) ?>?novo">Cadastrar Associado</a>
          <a target="_blank" href="<?php echo get_permalink(881) ?>?novo">Cadastrar Curriculo</a>
        </div>
      </div>
    </div>
    <script>
      jQuery(document).ready(function($) {
        $('#welcome-panel').after($('#custom-id').show());
      });
    </script>

  <?php }
/* ----------------------------------------- Register Dashboard Widget */     



/* Menu item p/ Curriculus */
/* ----------------------------------------- */
add_action( 'admin_menu', 'register_custom_menu_link' );
/**
 * @author    Brad Dalton
 * @example   http://wpsites.net/wordpress-admin/add-top-level-custom-admin-menu-link-in-dashboard-to-any-url/
 * @copyright 2014 WP Sites
 */
function register_custom_menu_link(){
    add_menu_page( 'Curriculos', 'Curriculos', 'manage_options', 'edit.php?s&post_status=all&post_type=wpcf7s&action=-1&m=0&wpcf7_contact_form=4969&filter_action=Filtrar&paged=1&action2=-1', null, 'dashicons-external', null );     
}
/* ----------------------------------------- Menu item p/ Curriculus */

