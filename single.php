<?php get_header(); ?>
	<main id="main-content" class="main" role="main">		

		<div class="container">	

			<div class="row">				
			    
		    <div id="content" class="content col-md-8">
		        	<?php 
		        		if (have_posts()): the_post();
		        			if ( function_exists('yoast_breadcrumb') ) { 
		        					yoast_breadcrumb('<p id="breadcrumbs">','</p>'); 
		        			}
		        	?>
		        	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>						
		        		
		        		<h1 class="page-title color-pri"><?php the_title(); ?></h1>
		        		
		        		<ul class="infos">
		        			<li>Por Secovi Rio<?php // echo get_the_author_meta('display_name'); ?> - <time><?php echo get_the_date('d/m/Y H:i'); ?></time></li>
		        		</ul>					
		        		<br><br>	

		        		<?php the_content(); ?>
		        		
		        		<br> <br>
		        		<!-- <a href="<?php echo home_url(); ?>/blog" title="Voltar" class="btn btn-default btn-voltar">< voltar</a> -->
		        		<!-- <br> <br> <br> -->
								<?php if ($post->post_type == 'post'): ?>
									<?php get_block('_comments-disqus') ?>
									<?php include('page-templates/partials/_related-posts-single.php'); ?>
								<?php endif ?>		        		

		        	</article>

		        <?php endif; ?>
		    </div>	

    		<aside id="sidebar" class="sidebar col-md-4">
            <div class="sidebar__inner">            
                <?php 
                	if ( is_active_sidebar( 'sidebar-principal' ) ) :
                			dynamic_sidebar( 'sidebar-principal' );
                	endif;
                ?>
            </div>
        </aside>

			</div> <!-- row -->

		</div> <!-- container -->

	</main>
	
	<?php 
		// Esconde o breadcrumb "Revista Digital" nos clippings
		if (has_category('clipping', get_the_id())) {
			echo '<style type="text/css" media="screen">#breadcrumbs > span > span > span > a, #breadcrumbs > span > span > span > a + i { display: none; }</style>';
		}
	 ?>




<?php get_footer(); ?>