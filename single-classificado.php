<?php get_header(); ?>
	<main id="main-content" class="main classificados categorias classificado" role="main">
		<?php get_block('_classificados-menu'); ?>
		<div class="classificado-wrapper">
			<div class="container-fluid">
				<?php
					if (have_posts()): the_post();
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						}
				?>
					<div class="row">
						<div class="col-12 col-md-7">
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								<h2><?php echo get_field('nome'); ?></h2>
								
								<?php if (get_field('telefone_ddd') && get_field('telefone_num')) : ?>							
									<p title="(<?php echo get_field('telefone_ddd'); ?>) <?php echo get_field('telefone_num'); ?>" class="tel">(<?php echo get_field('telefone_ddd'); ?>) <?php echo get_field('telefone_num'); ?></p>
								<?php endif; ?>						
								
								<?php if (get_field('whatsapp_num')) : ?>
									<p title="(<?php echo get_field('whatsapp_ddd'); ?>) <?php echo get_field('whatsapp_num'); ?>" class="wpp">(<?php echo get_field('whatsapp_ddd'); ?>) <?php echo get_field('whatsapp_num'); ?></p>
								<?php endif; ?>

								<p title="<?php echo get_field('email'); ?>" class="email"><?php echo get_field('email'); ?></p>
								
								<?php if (get_field('site')) : ?>
									<a href="<?php echo get_field('site'); ?>" target="_blank" class="site"><?php echo get_field('site'); ?></a>
								<?php endif; ?>
								
								<?php if (get_field('imagem')) : ?>
									<div class="image-wrapper">
										<img src="<?php echo get_field('imagem')['url']; ?>" alt="<?php echo get_field('imagem')['alt']; ?>">
									</div>
								<?php endif; ?>
								
								<h4>Descrição</h4>
								<p class="desc"><?php echo get_field('descricao', false, false); ?></p>
								<?php print_anuncio_tags_list() ; ?>
							</article>
						</div>

						<div class="col-12 col-md-4">
							<aside>
								<h2>Enviar mensagem</h2>
								<?php echo do_shortcode('[contact-form-7 id="8596" title="Mensagem de um classificado"]'); ?>
							</aside>
						</div>

					</div>
				<?php endif; ?>
			</div>
		</div>
		<?php 
			$terms = get_the_terms( $post->ID, 'tipo' );
			if ( !empty( $terms ) ){
				$term = array_shift( $terms );
				print_carousel_anuncios_relacionados($term->slug);
			}
		?>
	</main>
<?php get_footer(); ?>