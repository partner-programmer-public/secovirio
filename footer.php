<?php $homeID = get_option( 'page_on_front' ); ?>
	<footer id="footer" role="contentinfo">		
		<div id="newsletter">
			<div class="container">
				<h3>RECEBA NOSSAS NOVIDADES</h3>
				<?php echo do_shortcode('[contact-form-7 id="109" title="Receba nossas novidades" html_class="form-inline justify-content-center align-items-start"]') ?>
			</div>
		</div>
		<div id="infos" class="container pb-4">			
			<div class="row">
				<div class="col-12 col-sm-6 col-lg-3 order-11 order-lg-1 mx-auto">
					<a class="logotipo logotipo-rodape" href="http://secovis.com.br/" target="_blank" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php echo get_field('logotipo_rodape', $homeID); ?>" class="img-fluid" alt="Secovis Brasil"/>
					</a>
				</div>
				<div class="col-12 col-sm-4 col-lg-3 order-1">
					<?php 
						echo '<h4>Acesse também:</h4>';
					  wp_nav_menu( array(         
					    'theme_location'    => 'local',
					    'depth'             => 2,
					    'container'         => 'div',
					    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
					    'walker'            => new WP_Bootstrap_Navwalker()
					  )); 
					?>      
				</div>
				<div class="col-12  col-sm-4 col-lg-3 order-1">
					<h4 style="margin-bottom: -25px;">ONDE ESTAMOS</h4>
					<h4>SEDE</h4>
					<p class="endereco_info" style="margin-bottom: 0px;"><?php echo get_field('endereco_info',get_option( 'page_on_front' ));?></br></p>
					<h4>REGIONAIS</h4>
					<p class="endereco_info" style="margin-bottom: 0px;"><a href="<?php echo home_url() ?>/quem-somos/regionais/" style="margin-top: -10px;">Clique aqui e conheça nossa abrangência</a></p>
				</div>
				<div class="col-12  col-sm-4 col-lg-3 order-1">
					<h4>Telefone:</h4>
					<?php
						$tels = get_field('telefone_info', $homeID);
						if ($tels) {
              echo '<ul class="list-inline">';
							$i=1; foreach ($tels as $tel) {
								$fullNumber = $tel['ddd'] . $tel['numero'];
								$icone = $tel['icone'] ? $tel['icone'] : '';								
								$class = ($i==1) ? 'mr-4' : '' ;
								
								echo 	'<li class="list-inline-item '.$class.'"><a class="telefone_info"  title="Ligue para nós!" href="tel:'.$fullNumber.'">',
												'<small>'.$tel['ddd'].'</small>'.$tel['numero'],
												$icone,
											'</a></li>';
              $i++; }
              echo '</ul>';
						}
					?>
				</div>
				<div class="col-12 text-center order-12">
          <?php if (is_front_page() || is_page(77)) : ?>
            <a title="Design por React" href="https://react.ag/" target="_blank">
              <p class="m-0 py-3">React - Agência de marketing digital</p>
            </a>
          <?php else : ?>
            <p class="m-0 py-3">React - Agência de marketing digital</p>
          <?php endif; ?>
				</div>
			</div>
			
		</div>		
  </footer>
  <?php if ( !get_field('ocultar_pop_up') ) : ?>
    <?php if ( is_front_page() ) : ?>
      <style type="text/css" rel="stylesheet">
        .main-popup {
          position: fixed;
          left: 0; bottom: 0;
          z-index: 99999;
          display: flex;
          flex-direction: column;
          align-items: center;
          width: 600px; height: 435px;
          padding: 1.6rem;
          box-shadow: 0px 0px 8px #333 !important;
          border: 1px solid #BBBBBB;
          background-color: #003470;
          color: white;
          text-align: center;
          transition: all 150ms ease-in-out;
        }
        .main-popup h1 {
          color: white;
          font-size: 18px;
        }
        .main-popup h2 {
          color: white;
          font-size: 27px;
          margin-bottom: 20px;
        }
        .main-popup img {
          width: 155px;
          margin-bottom: 20px;
        }
        .main-popup .btn {
          background-color: #dcad77;
          text-transform: uppercase;
          color: white;
          border-radius: 0;
          font-size: 17px;
          padding: .7rem 2.5rem;
          margin-bottom: 10px;
        }

        .main-popup .btn-close.icon {
          width: 15px;
          height: 15px;
          display: block;
          padding: 0;
          position: absolute;
          right: 10px;
          top: 10px;
          border: 0;
          background: transparent;
          color: white;
          cursor: pointer;
          z-index: 10;
        }

        .main-popup .btn-close.icon svg {
          fill: white;
        }

        .main-popup .btn-close {
          color: white;
        }

        .main-popup .btn-close:hover {
          text-decoration: none;
        }
      </style>
      
      <div class="main-popup">
        <button class="btn-close icon">
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 348.333 348.334">
            <title>Fechar</title>
            <path d="M336.559,68.611L231.016,174.165l105.543,105.549c15.699,15.705,15.699,41.145,0,56.85
                c-7.844,7.844-18.128,11.769-28.407,11.769c-10.296,0-20.581-3.919-28.419-11.769L174.167,231.003L68.609,336.563
                c-7.843,7.844-18.128,11.769-28.416,11.769c-10.285,0-20.563-3.919-28.413-11.769c-15.699-15.698-15.699-41.139,0-56.85
                l105.54-105.549L11.774,68.611c-15.699-15.699-15.699-41.145,0-56.844c15.696-15.687,41.127-15.687,56.829,0l105.563,105.554
                L279.721,11.767c15.705-15.687,41.139-15.687,56.832,0C352.258,27.466,352.258,52.912,336.559,68.611z"></path>
          </svg>
        </button>
        <h1><?php the_field('title_popup', get_option('page_on_front')); ?></h1>
        <h2><?php the_field('subtitle_popup', get_option('page_on_front')); ?></h2>
        <img src="<?php the_field('thumb_popup', get_option('page_on_front')); ?>" alt="<?php echo get_field('title_popup', get_option('page_on_front')).' '.get_field('subtitle_popup', get_option('page_on_front')); ?>" class="img-fluid">
        <?php if ( get_field('btn_popup', get_option('page_on_front')) ) : ?>
          <a href="<?php echo get_field('btn_popup', get_option('page_on_front'))['url']; ?>" target="<?php echo get_field('btn_popup', get_option('page_on_front'))['target']; ?>" class="btn"><?php echo get_field('btn_popup', get_option('page_on_front'))['title']; ?></a>
        <?php endif; ?>
        <a href="#" class="btn-close">Não tenho interesse</a>
      </div>

      <script type="text/javascript">
        jQuery(document).ready(function(){
          setTimeout(function() { 
            jQuery('.main-popup').removeClass('d-lg-none');
            jQuery('.main-popup').addClass('d-lg-flex');
        }, 2000);
        });
        jQuery('.main-popup .btn-close').click(function (e) { 
          e.preventDefault();
          jQuery('.main-popup').removeClass('d-lg-flex');
          jQuery('.main-popup').addClass('d-lg-none');
        });
      </script>
    <?php endif; ?>
  <?php endif; ?>
	
	<a href="#main-content" class="smoothscroll" id="scroll-top"><i class="fa fa-angle-up fa-fw"></i></a>
<?php wp_footer(); ?>

<div class="gprd-wrap">
  <div class="container-fluid">
    <div class="row">
    <div class="col-12 col-sm-4 col-m-4 col-lg-8 col-xl-8">
      <p>Utilizamos cookies para personalizar sua experiência. Ao continuar a visitar este site, você concorda com o uso de cookies.</p>
    </div>
    <div class="col-12 col-sm-4 col-m-4 col-lg-2 col-xl-1">
      <a class="accept-btn" href="#">Aceitar</a>
    </div>
    <div class="col-12 col-sm-4 col-m-4 col-lg-2 col-xl-1">
      <a class="read-more" href="<?php echo home_url(); ?>/politica-de-privacidade">Leia Mais</a>
    </div>
    </div>
  </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>
<script>
  if (Cookies.get('secovi-gprd') != '1') {
    document.querySelector(".accept-btn").addEventListener('click',function (){
      Cookies.set('secovi-gprd', '1', { expires: 365 })
      document.querySelector(".gprd-wrap").classList.add('hide')
    });
  } else if (Cookies.get('secovi-gprd') == '1') {
    document.querySelector(".gprd-wrap").classList.add('hide')
  }
</script>

<!-- Start of  Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=79a5604b-2494-455a-96d7-b5df0d3b821a"> </script>
<!-- End of  Zendesk Widget script -->
<script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/b858c8d5-c5ab-40df-9347-d3bac7270a47-loader.js" ></script>

</body>
</html>
