<?php // echo get_page_template_slug(); ?>
<?php get_header(); ?>
	<main id="main-content" class="main" role="main">		

		<div class="container">	

			<div class="row">				

		    <div id="content" class="content col-md-8">
		    		<?php 		    			
		    			if (is_page_template('page-templates/page-convencoes-coletivas.php')) {
		    				get_partial('loop-convencoes-coletivas');
		    			
		    			} elseif (is_page_template('page-templates/page-pesquisar-curriculum.php')) {
		    				get_partial('loop-pesquisar-curriculos');
		    			
		    			} elseif (is_page_template('page-templates/page-formularios.php')) {
		    				
		    				if (is_user_logged_in() || isset($_GET['novo'])) {
	    						get_partial('fornecedores-formularios');
	    					} else {
	    						get_partial('_login');
	    					}
		    			
		    			} elseif (is_page_template('page-templates/page-fornecedores.php')) {
	    					
	    					if (is_user_logged_in() && isset($_GET['novo'])) {
	    						get_partial('fornecedores-formularios');
	    					} else {
	    						get_partial('_login');
	    					}
		    				
		    			} elseif(is_post_type_archive('agenda')) {
		    				get_partial('loop-cursos-e-eventos');
		    			} elseif (is_tax('cat_legislacao')) {
		    				get_partial('loop-cat-legislacao');
		    			} elseif (is_author()) {
		    				get_partial('loop-author');
		    			} elseif (is_search()) {
		    				get_partial('loop-search');
		    			} else {
		    				get_partial('loop-index');
		    			}
		    		 ?>		    		
		    </div>	

    		<aside id="sidebar" class="sidebar col-md-4">
            <div class="sidebar__inner">            
                <?php 
                	if ( is_active_sidebar( 'sidebar-principal' ) ) :
                			dynamic_sidebar( 'sidebar-principal' );
                	endif;
                ?>
            </div>
        </aside>

			</div> <!-- row -->

		</div> <!-- container -->

	</main>
<?php get_footer(); ?>