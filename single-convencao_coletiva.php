<?php get_header(); ?>
	<main id="main-content" class="main" role="main">		

		<div class="container">	

			<div class="row">				
			    
		    <div id="content" class="content col-md-8">
		        	<?php 
		        		if (have_posts()): the_post();
		        			if ( function_exists('yoast_breadcrumb') ) { 
		        					yoast_breadcrumb('<p id="breadcrumbs">','</p>'); 
		        			}
		        	?>
		        	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>						
		        		
		        		
		        		<h1 class="page-title color-pri"><?php the_title(); ?></h1>
		        					
		        		<br><br>	

		        		<?php 
		        			$attID = get_post_meta( $post->ID, 'arquivo', 1 );		        			
		        			if ($attID) {
		        				$url = wp_get_attachment_url( $attID );
		        				echo do_shortcode('[botao-pdf titulo="para abrir o arquivo" url="'.$url.'" target="_blank"]');
		        			}
		        		 ?>
		        		
		        		<br> <br>
		        		<a href="<?php echo get_permalink(72) ?>" title="Voltar" class="btn btn-rosaclaro btn-icon btn-icon-left">
		        			<i class="fa fa-arrow-left bg-rosaescuro"></i> VOLTAR
		        		</a>
		        		<br> <br>

		        	</article>

		        <?php endif; ?>
		    </div>	

    		<aside id="sidebar" class="sidebar col-md-4">
            <div class="sidebar__inner">            
                <?php 
                	if ( is_active_sidebar( 'sidebar-principal' ) ) :
                			dynamic_sidebar( 'sidebar-principal' );
                	endif;
                ?>
            </div>
        </aside>

			</div> <!-- row -->

		</div> <!-- container -->

	</main>
<?php get_footer(); ?>