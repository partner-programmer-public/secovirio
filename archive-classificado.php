<?php 
$home_page_id = get_option( 'page_on_front' );
$classificados_page_id = get_field( 'pagina_classificados', $home_page_id);
wp_redirect( get_permalink($classificados_page_id), 301 );
exit();
?>